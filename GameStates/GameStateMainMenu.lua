--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : GameStateMainMenu.lua
Description : GameState that will handle the main menu of the game
Date        : 14/10/2016

--]]

GameStateMainMenu = {}

GameStateMainMenu_mt = {}

GameStateMainMenu_mt.__index = GameStateMainMenu

function GameStateMainMenu.New()

    local newobj = {
        -- Indicates if the gamestate has been loaded
        _loaded = false;

        -- The name of the gamestate
        _name = "GameStateMainMenu";

        _background = love.graphics.newImage("Resources/Pics/Menu_Background.png");

        _title = "The Escape";
        _menuText1 = G_GameInformations:GetText("Continuer");
        _menuText2 = G_GameInformations:GetText("Nouveau");
        _menuText3 = G_GameInformations:GetText("Quitter");
        _menuText4 = G_GameInformations:GetText("Réinitiliser la sauvegarde");

        _videoFinished = false;

        _videoFinishedTime = 0 ;

    }

    if love.getVersion() > 10 then
        newobj._music = love.audio.newSource("Resources/Sounds/Musics/The Escape - Main Theme.wav" , "stream");
    else 
        newobj._music = love.audio.newSource("Resources/Sounds/Musics/The Escape - Main Theme.wav");
    end

    if love.getVersion() > 10 then
        newobj._video = love.graphics.newVideo("Resources/TheEscape_Menu.ogv",{audio = false}) ;
    else 
        newobj._video = love.graphics.newVideo("Resources/TheEscape_Menu.ogv",false) ;
    end


    setmetatable(newobj,GameStateMainMenu_mt)
    return newobj

end

function GameStateMainMenu.IsLoaded(self)
    return self._loaded
end

function GameStateMainMenu.GetName(self)
    return self._name
end

function GameStateMainMenu.Load(self)

    -- Change the loaded state of the gamestate
    self._loaded = true
    -- Change the loaded state of the gamestate
    self._w = G_GameInformations:GetWidth()
    self._h = G_GameInformations:GetHeight()
    self._scx = G_GameInformations:GetWidth()/2048
    self._scy = G_GameInformations:GetHeight()/1024
    
    love.graphics.setFont(G_GameInformations._fontText)
    self._music:play(1)

    self._t1x = ((self._w/2)/2)-(#self._menuText1*12)/2
    self._t2x = ((self._w/2))-(#self._menuText2*12)/2
    self._t3x = ((self._w/2)+((self._w/2)/2))-(#self._menuText3*12)/2
    self._t4x = ((self._w/2)+24) - ((#self._menuText4*12)/2)

    self._video:play()

end

function GameStateMainMenu.MousePressed(self,x,y,button)
    if button == 1 then
        if x >= self._t1x and x <= self._t1x + (#self._menuText1*12) and y >= 400 and y <= 450 and G_GameInformations:GetLevelSaved() ~= "Level_1" then
            self._music:stop()
            G_GameStateManager:Push("GameStateGame")
            -- self._videoFinished = false 
            -- self._videoFinishedTime = 0
            self._video:play()
        end
        if x >= self._t2x and x <= self._t2x + (#self._menuText2*12) and y >= 400 and y <= 450 then
            G_GameInformations._savedLevel = "Level_1"
            G_GameInformations._additionalFromMenu = "NewGame"
            self._music:stop()
            G_GameStateManager:Push("GameStateGame")
            -- self._videoFinished = false 
            -- self._videoFinishedTime = 0
            self._video:play()
        end
        if x >= self._t3x and x <= self._t3x + (#self._menuText3*12) and y >= 400 and y <= 450 then
            self._music:stop()

            self._music = nil 
            G_GameStateManager:Get("GameStateGenerique")._video =nil 
            
            if G_GameStateManager:Get("GameStateGame")._mp then 
                G_GameStateManager:Get("GameStateGame")._mp:Dispose()
                G_GameStateManager:Get("GameStateGame")._mp = nil 
            end

            love.event.quit()
        end
        if x >= self._t4x and x <= self._t4x + (#self._menuText4*12) and y >= 650 and y <= 700 then
            G_GameInformations:ResetSave()
        end
    else
    end
end

function GameStateMainMenu.Update(self,dt)
    if not self._video:isPlaying() then
        if self._videoFinished == false then 
            self._videoFinished = true 
            self._videoFinishedTime = 1000
        end
    end 

    if self._videoFinished == true then 
        if self._videoFinishedTime <= 0 then 
            self._video:rewind()
            self._video:play()
            self._videoFinished = false
        else 
            self._videoFinishedTime = self._videoFinishedTime - 1
        end
    end
    self._music:play()
end

function GameStateMainMenu.KeyPressed(self,key,scancode,isrepeat)

end

function GameStateMainMenu.Draw(self)

    self._video:play()

    self._w = G_GameInformations:GetWidth()
    self._h = G_GameInformations:GetHeight()
    self._scx = G_GameInformations:GetWidth()/2048
    self._scy = G_GameInformations:GetHeight()/1024

    self._menuText1 = G_GameInformations:GetText("Continuer");
    self._menuText2 = G_GameInformations:GetText("Nouveau");
    self._menuText3 = G_GameInformations:GetText("Quitter");
    self._menuText4 = G_GameInformations:GetText("Réinitiliser la sauvegarde");

    self._t1x = ((self._w/2)/2)-(#self._menuText1*12)/2
    self._t2x = ((self._w/2))-(#self._menuText2*12)/2
    self._t3x = ((self._w/2)+((self._w/2)/2))-(#self._menuText3*12)/2
    self._t4x = ((self._w/2)+24) - ((#self._menuText4*12)/2)

    x = love.mouse.getX()
    y = love.mouse.getY()

    -- love.graphics.draw(self._background,0,0,0,self._scx,self._scy)
    --love.graphics.print(self._title,(self._w/2)-(#self._title*20),100,0,2,2)
    love.graphics.draw(self._video,(G_GameInformations:GetWidth() - self._video:getWidth())/2,(G_GameInformations:GetHeight() - self._video:getHeight())/2 , 0 , 1 , 1)

    if x >= self._t1x and x <= self._t1x + (#self._menuText1*12) and y >= 400 and y <= 450 then
        love.graphics.setColor(0,200,0)
    end
        love.graphics.print(self._menuText1,self._t1x ,400)
        love.graphics.setColor(255,255,255)
    if x >= self._t2x and x <= self._t2x + (#self._menuText2*12) and y >= 400 and y <= 450 then
        love.graphics.setColor(0,200,0)
    end
        love.graphics.print(self._menuText2,self._t2x,400)
        love.graphics.setColor(255,255,255)
    if x >= self._t3x and x <= self._t3x + (#self._menuText3*12) and y >= 400 and y <= 450 then
        love.graphics.setColor(0,200,0)
    end
        love.graphics.print(self._menuText3,self._t3x,400)
        love.graphics.setColor(255,255,255)

    if G_GameInformations:CountSecret() >= 1 then 
        if x >= self._t4x and x <= self._t4x + (#self._menuText4*12) and y >= 650 and y <= 700 then
            love.graphics.setColor(0,200,0)
        end
            love.graphics.print(self._menuText4,self._t4x,650)
            love.graphics.setColor(255,255,255)
    end


    love.graphics.print("Dissident Studio - The Escape © - V1.3 Remastered",20,self._h-30,0,0.7,0.7)
    -- love.graphics.print("2016 - All rights reserved",20,self._h-60,0,0.7,0.7)
    -- love.graphics.print("Version 1.0 - " .. love.system.getOS(),20,self._h-20,0,0.7,0.7)

end
