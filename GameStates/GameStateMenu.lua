--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : GameStateMenu.lua
Description : GameState that will handle the main menu of the game
Date        : 14/10/2016

--]]

GameStateMenu = {}

GameStateMenu_mt = {}

GameStateMenu_mt.__index = GameStateMenu

G_GameChangingLevel = {}
G_GameChangingLevel._is = false
G_GameChangingLevel._level = "none"

function GameStateMenu.New()

    local newobj = {
        _teEditor = true;

        -- Indicates if the gamestate has been loaded
        _loaded = false;

        -- The name of the gamestate
        _name = "GameStateMenu";

        -- Store the time of the game state
        _time = 0;

        -- The mask that will be applied on all the sprites
        _mask = love.graphics.newImage("Resources/Pics/Shadow.png")
    }

    setmetatable(newobj,GameStateMenu_mt)
    return newobj

end

function GameStateMenu.IsLoaded(self)
    return self._loaded
end

function GameStateMenu.GetName(self)
    return self._name
end

function GameStateMenu.Load(self)
    -- Change the loaded state of the gamestate
    self._loaded = true
    -- Change the loaded state of the gamestate

end

function GameStateMenu.MousePressed(self,x,y,button,istouch)
    if button == 1 then
        -- x,y = G_GameInfo:InvertTransformFromDisplay(x,y)
        G_GameStateManager:Push("GameStateGame")
    elseif button == 2 then
        self._mp:Action(x,y)
    end
end

function GameStateMenu.Update(self,dt)

end

function GameStateMenu.KeyPressed(self,key,scancode,isrepeat)

end

function GameStateMenu.Draw(self)
end
