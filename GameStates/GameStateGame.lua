--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : GameStateGame.lua
Description : GameState that will handle the main menu of the game
Date        : 14/10/2016

--]]



GameStateGame = {}

GameStateGame_mt = {}

GameStateGame_mt.__index = GameStateGame

G_GameChangingLevel = {}
G_GameChangingLevel._is = false
G_GameChangingLevel._level = "none"

function GameStateGame.New()

    local newobj = {
        _teEditor = true;

        -- Indicates if the gamestate has been loaded
        _loaded = false;

        -- The name of the gamestate
        _name = "GameStateGame";

        -- Store the time of the game state
        _time = 0;

        -- The mask that will be applied on all the sprites
        _mask = love.graphics.newImage("Resources/Pics/Shadow.png");

        _footCursor = love.graphics.newImage("Resources/Pics/Feet_Resized.png");
    }

    setmetatable(newobj,GameStateGame_mt)
    return newobj

end

function GameStateGame.IsLoaded(self)
    return self._loaded
end

function GameStateGame.GetName(self)
    return self._name
end

function GameStateGame.Load(self)
    -- Change the loaded state of the gamestate
    self._loaded = true
    -- Change the loaded state of the gamestate

    self._mp = Map.New()
    self._mp:Load(G_GameInformations:GetLevelSaved())

end

function GameStateGame.MousePressed(self,x,y,button)
    if not self._mp:Action(x,y) then 
        x,y = G_GameInformations:InvertTransformFromDisplay(x,y)
        if x <= 200 and y >= 200 and y <= 300 then  self._mp:Move("Left") end
        if x >= 300 and y >= 200 and y <= 300 then  self._mp:Move("Right") end
        if y <= 200 and x >= 200 and x <= 300 then  self._mp:Move("Up") end
        if y >= 300 and x >= 200 and x <= 300 then  self._mp:Move("Down") end
    end
    -- if button == 1 then
    --     x,y = G_GameInformations:InvertTransformFromDisplay(x,y)
    --     if x <= 200 and y >= 200 and y <= 300 then  self._mp:Move("Left") end
    --     if x >= 300 and y >= 200 and y <= 300 then  self._mp:Move("Right") end
    --     if y <= 200 and x >= 200 and x <= 300 then  self._mp:Move("Up") end
    --     if y >= 300 and x >= 200 and x <= 300 then  self._mp:Move("Down") end
    -- elseif button == 2 then
    --     self._mp:Action(x,y)
    -- end
end

function GameStateGame.Update(self,dt)

    if G_GameInformations._additionalFromMenu == "NewGame" then
        self._mp:ChangeLevel("Level_1") ;
        G_GameInformations._additionalFromMenu = ""
    end

    if G_GameChangingLevel._is == true then

        local tmp_picking_text = self._mp._pickingText
        local tmp_picking_time = self._mp._pickingTime
        self._mp:Dispose()
        self._mp = nil

        self._mp = Map.New()
        self._mp:Load(G_GameChangingLevel._level)

        self._mp._pickingText = tmp_picking_text
        self._mp._pickingTime = tmp_picking_time

        G_GameChangingLevel._is = false
        G_GameChangingLevel._level = "none"
    end

    self._mp:Update(dt)

end

function GameStateGame.TextInput(self,t)
    self._mp:TextInput(t)
end

function GameStateGame.KeyPressed(self,key,scancode,isrepeat)
    if key == "escape" then self._mp._musics[1]:pause() ; G_GameStateManager:Push("GameStateMainMenu") ; self._mp._soundAlert:pause() end
    if key == "backspace" then self._mp._objectsManager:Backspace() end
end

function GameStateGame.Draw(self)

    if not G_GameChangingLevel._is then 
        self._mp:Draw()
    end

end
