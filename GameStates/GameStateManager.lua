--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : GameStateManager.lua
Description : A class to manage gamestates
Date        : 15/10/2016

--]]

-- Requiring all the gamestates
require "GameStates/GameStateSplash"
-- Requiring all the gamestates



GameStateManager = {}

GameStateManager_mt = {}

GameStateManager_mt.__index = GameStateManager

function GameStateManager.New()

    local newobj = {

        -- The actual game state that is run
        _actual = nil;

        -- All the game states
        _gameStates = {} ;

        _chromaSwitchTime = 0 ;

        _buzs = {} ;

    }

    setmetatable(newobj,GameStateManager_mt)

    for i = 1 , 5 do 

        if love.getVersion() > 10 then
            buz = love.audio.newSource("Resources/Sounds/Sounds/buz_" .. i .. ".mp3" , "static")
        else
            buz = love.audio.newSource("Resources/Sounds/Sounds/buz_" .. i .. ".mp3")
        end
        buz:setVolume(0.1)
        table.insert(newobj._buzs , buz)

    end

    return newobj

end

function GameStateManager.Add(self,...)

    local arg = {...}
    for i,v in ipairs(arg) do
        if v ~= nil then
            table.insert(self._gameStates,v)
        end
    end

end

function GameStateManager.Get(self , gs)

    for i,v in ipairs(self._gameStates) do
        if v:GetName() == gs then
            return v
        end
    end

end

function GameStateManager.Push(self,gamestate)
    for i,v in ipairs(self._gameStates) do
        if v:GetName() == gamestate then
            self._actual = v
            -- If the gamestate is not loaded, we load it !
            if v:IsLoaded() == false then
                v:Load()
            end
            return
        end
    end

    -- If we are here, means that the gamestate does not exist
    DS_ERROR("GameState " .. gamestate .. " does not exists or has not been added in Manager ! ")

end

function GameStateManager.Stop(self)

    -- Deleting all gamestates
    for i,v in ipairs(self._gameStates) do
        self._gameStates[i] = nil
    end

    -- Garbage collecting
    -- collectgarbage("collect")

    -- Exiting love
    love.event.quit()

end

function GameStateManager.GetNumberOfGameStates(self)
    return #self._gameStates
end

-- From there to the end we define all the callback functions of LOVE2D

function GameStateManager.Load(self)
    if self._actual.Load ~= nil then
        self._actual:Load()
    end
end

function GameStateManager.Update(self,dt)

    if G_Chroma then
        if self._chromaSwitchTime <= 0 then 
            G_Chroma.angle = 0
            G_Chroma.radius = 0
            G_Scanlines._opacity = 0.3       
            if math.random(0,200) == 5 then 
                self._chromaSwitchTime = math.random(10,30)
                self._buzs[math.random(1,5)]:play()
            end 
        else 
            G_Chroma.angle = math.random(0.0,3.1)
            G_Chroma.radius = math.random(0,4)
            G_Scanlines._opacity = math.random(0.3,1.0)  
        end
        self._chromaSwitchTime = self._chromaSwitchTime - 1
    end


    if self._actual.Update ~= nil then
        self._actual:Update(dt)
    end
end

function GameStateManager.Draw(self)
    if self._actual.Draw ~= nil then
        self._actual:Draw()
    end
end

function GameStateManager.KeyPressed(self,key,scancode,isrepeat)
    if self._actual.KeyPressed ~= nil then
        self._actual:KeyPressed(key,scancode,isrepeat)
    end

    -- Here we call the set fullscreen of the G_GameInformations
    if key == "f11" then
        G_GameInformations:SetFullScreen()
    end

    -- if key == "f" then 
    --     G_Lang = "fr"
    -- end 
    -- if key == "e" then 
    --     G_Lang = "en"
    -- end

end

function GameStateManager.KeyReleased(self,key,scancode)
    if self._actual.KeyReleased ~= nil then
        self._actual:KeyReleased(key,scancode)
    end
end

function GameStateManager.MousePressed(self,x,y,button,istouch)
    if self._actual.MousePressed ~= nil then
        self._actual:MousePressed(x,y,button,istouch)
    end
end

function GameStateManager.MouseReleased(self,x,y,button,istouch)
    if self._actual.MouseReleased ~= nil then
        self._actual:MouseReleased(x,y,button,istouch)
    end
end

function GameStateManager.WheelMoved(self,x,y)
    if self._actual.WheelMoved ~= nil then
        self._actual:WheelMoved(x,y)
    end
end

function GameStateManager.FileDropped(self,file)
    if self._actual.FileDropped ~= nil then
        self._actual:FileDropped(file)
    end
end

function GameStateManager.Resize(self,w,h)
    G_GameInformations.m_width = w
    G_GameInformations.m_height = h
    if self._actual.Resize ~= nil then
        self._actual:Resize(w,h)
    end
end

function GameStateManager.TextInput(self,t)
    if self._actual.TextInput ~= nil then
        self._actual:TextInput(t)
    end
end
