

require "editor/EditorMap"
require "editor/EditorObjects"


GameStateEditorV2 = {}

GameStateEditorV2_mt = {}

GameStateEditorV2_mt.__index = GameStateEditorV2


function GameStateEditorV2.New()

    local newobj = {

        -- The actual level
        m_actualLevel = "Level_16" ;

        -- The values of the actual level
        m_map = EditorMap.New() ;

        -- 
        m_objects = EditorObjects.New() ;

        m_loaded = false ;


        m_state = "map" ;

        m_stateMap = {actual = 1 };


    }

    setmetatable(newobj , GameStateEditorV2_mt)

    return newobj 

end 

function GameStateEditorV2.GetName(self)

    return "GameStateEditorV2"

end

function GameStateEditorV2.Load(self)

    self:LoadLevel(self.m_actualLevel)

    love.window.setMode(960,600,{resizable = true})

    self.m_loaded = true

end 

function GameStateEditorV2.IsLoaded(self)

    return self.m_loaded

end

function GameStateEditorV2.LoadLevel(self , levelname)

    self.m_map:Load(levelname)

    self.m_objects:Load(levelname)

end

function GameStateEditorV2.FileDropped(self , file)




end

function GameStateEditorV2.KeyPressed(self , key )

    if key == "left" then 
        self.m_map:Scroll(-32 , 0)
        self.m_objects:Scroll(-32 , 0)
    end 
    if key == "right" then 
        self.m_map:Scroll(32 , 0)
        self.m_objects:Scroll(32 , 0)
    end 
    if key == "up" then 
        self.m_map:Scroll(0 , -32)
        self.m_objects:Scroll(0 , -32)
    end 
    if key == "down" then 
        self.m_map:Scroll(0 , 32)
        self.m_objects:Scroll(0 , 32)
    end 

end 

function GameStateEditorV2.WheelMoved(self , x , y)

    if self.m_state == "map" then 
        if y > 0 then 
            if self.m_stateMap.actual < G_GameInformations:GetNumberOfRooms() then 
                self.m_stateMap.actual = self.m_stateMap.actual + 1
            end 
        else 
            if self.m_stateMap.actual > 1 then 
                self.m_stateMap.actual = self.m_stateMap.actual - 1
            end 
        end 
    end 

end

function GameStateEditorV2.MousePressed(self , button)

    if self.m_state == "map" then 

        local x , y = 0
        x = math.floor( ( love.mouse.getX() - self.m_map.m_scrollX ) / 256) 
        y = math.floor( ( love.mouse.getY() - self.m_map.m_scrollY ) / 256) 
        self.m_map.m_values[y][x] = self.m_stateMap.actual-1

    end 

end 


function GameStateEditorV2.Draw(self)


    self.m_map:Draw()

    self.m_objects:Draw()



    if self.m_state == "map" then 

        love.graphics.draw(self.m_map.m_roomsPictures[self.m_stateMap.actual] , love.mouse.getX() , love.mouse.getY() , 0 , 0.2 , 0.2)

    end


    if self.m_state == "map" then 

        local x , y = 0
        x = math.floor( ( love.mouse.getX() + self.m_map.m_scrollX ) / 256) 
        y = math.floor( ( love.mouse.getY() + self.m_map.m_scrollY ) / 256) 
        love.graphics.print("X = " .. x .. " / Y = " .. y)

    end 

end

