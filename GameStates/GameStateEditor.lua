--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : GameStateEditor.lua
Description : GameState that will handle the main menu of the game
Date        : 14/10/2016

--]]

GameStateEditor = {}

GameStateEditor_mt = {}

GameStateEditor_mt.__index = GameStateEditor

G_GameChangingLevel = {}
G_GameChangingLevel._is = false
G_GameChangingLevel._level = "none"

function GameStateEditor.New()

    local newobj = {
        -- Indicates if the gamestate has been loaded
        _loaded = false;

        -- The name of the gamestate
        _name = "GameStateEditor";

        -- Store the time of the game state
        _time = 0;

        _roomsPictures = {} ;
        _map = {} ;
        _actualRoom = 0;

        _filename = love.filesystem.getRealDirectory("main.lua") .. "/NewLevel.tem";

        _height = 0;
        _width = 0;
        _musicsName = {};
        _musics = {};
        _scrollX = 0;
        _scrollY = 0;

        _entryX = 0;
        _entryY = 0;


        -- All about the objects
        _objectsManager = ObjectsManager.New()


    }

    setmetatable(newobj,GameStateEditor_mt)
    return newobj

end

function GameStateEditor.IsLoaded(self)
    return self._loaded
end

function GameStateEditor.GetName(self)
    return self._name
end

function GameStateEditor.Load(self)
    -- Change the loaded state of the gamestate
    self._loaded = true
    -- Change the loaded state of the gamestate

    love.window.setTitle("The Escape - Editor <" .. self._filename .. ">*")

    for i=0, G_GameInformations:GetNumberOfRooms() do
        self._roomsPictures[i] = love.graphics.newImage("Resources/Pics/Rooms/Room_" .. i .. ".png")
    end

    for y=0,40 do
        self._map[y] = {}
        for x=0,40 do
            self._map[y][x] = 0
        end
    end


    -- G_GameInformations:ConfigureWindow()
    love.window.setMode(800,600 , {resizable = true})

end

function GameStateEditor.FileDropped(self,file)

    local hdle = love.filesystem.newFile("Data/".. file .. "/" .. file ..".tem")

    hdle:open("r")

    local json_data = G_Json.decode(tostring(hdle:read()))

    self._width = json_data.width 
    self._height = json_data.height

    local count = 1
    for y = 0 , self._height-1 do 
        self._map[y] = {}
        self._visitedValues[y] = {}
        for x = 0 , self._width-1 do 
        self._visitedValues[y][x] = 0
        self._map[y][x] = 0
            self._map[y][x] = json_data.map[count]
            count = count + 1
        end 
    end 

    self._x = json_data.entry_x 
    self._y = json_data.entry_y

    -- hdle = io.open(file:getFilename())
    -- self._filename = file:getFilename()
    -- DS_ASSERT(nil == hdle)

    -- buffer = hdle:read("*line")

    -- while buffer ~= nil do

    --     if not Misc_IsComment(buffer) and not Misc_IsEmpty(buffer) then

    --         if buffer == "map{" then
    --             -- Here we load the map
    --             for y = 0 , self._height-1 do
    --                 self._map[y] = {}
    --                 for x = 0 , self._width-1 do
    --                     self._map[y][x] = hdle:read("*number")
    --                 end
    --             end
    --         elseif buffer == "musics{" then
    --             while buffer ~= "}" do
    --                 if buffer ~= "}" and buffer ~= "musics{" then
    --                     table.insert(self._musicsName,buffer)
    --                     table.insert(self._musics,love.audio.newSource("Resources/Sounds/Musics/"..buffer..".wav"))
    --                 end
    --                 buffer = hdle:read("*line")
    --             end
    --         else
    --             if buffer ~= "}" then
    --                 key,value = Misc_KeyValue(buffer)
    --                 if key == "WIDTH" then self._width = tonumber(value) end
    --                 if key == "HEIGHT" then self._height = tonumber(value) end
    --                 if key == "ENTRY_X" then self._entryX = tonumber(value) end
    --                 if key == "ENTRY_Y" then self._entryY = tonumber(value) end
    --                 if key == "ALERT_TIME" then self._alert = true ; self._alertTime = tonumber(value) end
    --             end
    --         end
    --     end
    --     buffer = hdle:read("*line")
    -- end

end

function GameStateEditor.KeyPressed(self,key)
    if key == "left" and self._scrollX < self._width then self._scrollX = self._scrollX + 1 end
    if key == "right" and self._scrollX > 0 then self._scrollX = self._scrollX - 1 end
    if key == "up" and self._scrollY < self._height then self._scrollY = self._scrollY + 1 end
    if key == "down" and self._scrollY > 0 then self._scrollY = self._scrollY - 1 end
end

function GameStateEditor.Save(self)
    love.window.setTitle("The Escape - Editor <" .. self._filename .. ">")
    hdle = io.open(self._filename,"w+")

    hdle:write("WIDTH="..self._width.."\n")
    hdle:write("HEIGHT="..self._height.."\n")
    hdle:write("ENTRY_X="..self._entryX.."\n")
    hdle:write("ENTRY_Y="..self._entryY.."\n")
    if self._alert then
        hdle:write("ALERT_TIME=" .. self._alertTime .. "\n")
    end

    hdle:write("musics{\n")
    for i,v in ipairs(self._musicsName) do
        -- body...
        hdle:write(v .. "\n")
    end
    hdle:write("}\n")

    hdle:write("map{\n")
    for y=0,self._height-1 do
        -- body...
        for x=0,self._width-1 do
            -- body...
            hdle:write(self._map[y][x] .. " ")
        end
        hdle:write("\n")
    end
    hdle:write("}\n")
end

function GameStateEditor.MousePressed(self,x,y,button)

    if button == 1 then 

        x = math.floor(x / 128)-self._scrollX
        y = math.floor(y / 128)-self._scrollY
        if x > self._width or y > self._height then
            for y = self._height,self._height + 1 do
                for x = self._width,self._width + 1 do
                    self._map[y][x] = 0
                end
            end
        self._width = self._width + 1
        self._height = self._height + 1
        end
        self._map[y][x] = self._actualRoom

        love.window.setTitle("The Escape - Editor <" .. self._filename .. ">*")

    end


    if button == 2 then 
        gx = math.floor(love.mouse.getX() / 128)-self._scrollX
        gy = math.floor(love.mouse.getY() / 128)-self._scrollY
        rx = love.mouse.getX()-(gx*128)
        ry = love.mouse.getY()-(gy*128)
        rx = rx * 4
        ry = ry * 4
        -- error(self._filename:sub(0,#self._filename-3) .. "teo")
        hdle = io.open(self._filename:sub(0,#self._filename-3) .. "teo" , "a")
        hdle:write([[ 
ITEM{
ID=LVL_ITM_SECRET
PICTURE_NAME=Secret
GRID_X=]] .. gx .. [[  
GRID_Y=]] .. gy .. [[ 
ROOM_X=]] .. rx .. [[ 
ROOM_Y=]] .. ry .. [[ 
PICKABLE=True
}
            ]])
        hdle:close()
    end 

    if button == 3 then 
        gx = math.floor(love.mouse.getX() / 128)-self._scrollX
        gy = math.floor(love.mouse.getY() / 128)-self._scrollY
        rx = love.mouse.getX()-(gx*128)
        ry = love.mouse.getY()-(gy*128)
        rx = rx * 4
        ry = ry * 4
        -- error(self._filename:sub(0,#self._filename-3) .. "teo")
        hdle = io.open(self._filename:sub(0,#self._filename-3) .. "teo" , "a")
        hdle:write([[ 
NOTE{ 
ID=LVL_NOTE_01 
GRID_X=]] .. gx .. [[  
GRID_Y=]] .. gy .. [[ 
ROOM_X=]] .. rx .. [[ 
ROOM_Y=]] .. ry .. [[ 
CONTENT=Note_1 
}
]])
        hdle:close()

    end




end

function GameStateEditor.Update(self,dt)
    if love.keyboard.isDown("lctrl") and love.keyboard.isDown("s") then
        self:Save()
        love.timer.sleep(1)
    end
end

function GameStateEditor.WheelMoved(self,x,y)
    if y > 0 and self._actualRoom < G_GameInformations:GetNumberOfRooms() then self._actualRoom = self._actualRoom + 1 end
    if y < 0 and self._actualRoom >= 1  then self._actualRoom = self._actualRoom - 1 end

end

function GameStateEditor.Draw(self)
    for y=0,self._width-1 do
        for x=0,self._height-1 do
            if self._map[y][x] == nil then error("X:" .. x .. " Y:" .. y) end
            love.graphics.draw(self._roomsPictures[self._map[y+self._scrollY][x+self._scrollX]],x*128,y*128,0,0.25,0.25)
            love.graphics.print(x.."/"..y,x*128,y*128)
        end
    end

    love.graphics.draw(self._roomsPictures[self._actualRoom],love.mouse.getX(),love.mouse.getY(),0,0.25,0.25)
    gx = math.floor(love.mouse.getX() / 128)-self._scrollX
    gy = math.floor(love.mouse.getY() / 128)-self._scrollY
    rx = love.mouse.getX()-(gx*128)
    ry = love.mouse.getY()-(gy*128)
    rx = rx * 4
    ry = ry * 4
    love.graphics.print(rx.."/"..ry,love.mouse.getX()+64,love.mouse.getY()+64)
end
