--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : GameStateSplash.lua
Description : GameState that will handle the start of the game
Date        : 14/10/2016

--]]

GameStateSplash = {}

GameStateSplash_mt = {}

GameStateSplash_mt.__index = GameStateSplash

function GameStateSplash.New()

    local newobj = {
        -- Indicates if the gamestate has been loaded
        _loaded = false;

        -- The name of the gamestate
        _name = "GameStateSplash";

        -- Store the time of the game state
        _time = 0;

        -- If the game is in splash mode or in intro mode
        _isSplash = true;

        _intro3IMG = love.graphics.newImage("Resources/Pics/Intro/Intro_3.png");

        _burgerSplash = love.graphics.newImage("Resources/Pics/BurgerEngine_Splash.png") ;

        _frFlag = love.graphics.newImage("Resources/Pics/fr_flag.png");

        _ukFlag = love.graphics.newImage("Resources/Pics/uk_flag.png");
    }

    newobj._splashVideo = love.graphics.newVideo("Resources/Dissident_Studio_Splash.ogv")

    setmetatable(newobj,GameStateSplash_mt)
    return newobj

end

function GameStateSplash.IsLoaded(self)
    return self._loaded
end

function GameStateSplash.GetName(self)
    return self._name
end

function GameStateSplash.Load(self)
-- G_GameStateManager:Push("GameStateMainMenu")
    -- Change the loaded state of the gamestate
    self._loaded = true
    -- Change the loaded state of the gamestate

    if G_GameInformations.m_splash == true then
        love.window.setMode(G_GameInformations.m_splashPicture:getWidth(),G_GameInformations.m_splashPicture:getHeight(),
        {fullscreen = true , borderless="false" , resizable="false"})
    else
        self._time = 6
    end

end

function GameStateSplash.MousePressed(self , x , y)

    if self._time > 24 then
        if love.mouse.getX() >= (love.graphics.getWidth()/2) - 128 and love.mouse.getX() <= (love.graphics.getWidth()/2) + 128
        and love.mouse.getY() > (love.graphics.getHeight()/2) - 300 and love.mouse.getY() <= (love.graphics.getHeight()/2) - 44 then
            G_Lang = "fr"
            self:Dispose()
            G_GameStateManager:Push("GameStateMainMenu")
        end


        if love.mouse.getX() >= (love.graphics.getWidth()/2) - 128 and love.mouse.getX() <= (love.graphics.getWidth()/2) + 128
        and love.mouse.getY() > (love.graphics.getHeight()/2) and love.mouse.getY() <= (love.graphics.getHeight()/2) + 256 then
            G_Lang = "en"
            self:Dispose()
            G_GameStateManager:Push("GameStateMainMenu")
        end
    end

end

function GameStateSplash.Update(self,dt)
    self._time = self._time + dt
    if self._time >= 5 and self._isSplash == true then
        self._isSplash = false

        if G_GameInformations.m_fullscreen then 
            love.window.setMode(800,600,{fullscreen = true , fullscreentype = "desktop"})
            G_GameInformations.m_width = love.graphics.getWidth()
            G_GameInformations.m_height = love.graphics.getHeight()
        else 
            love.window.setMode(800,600)
        end

        self._scx = G_GameInformations:GetWidth()/1024
        self._scy = G_GameInformations:GetHeight()/768

        self._splashVideo:play()

        G_Scanlines = G_Shine.scanlines()
        G_Chroma = G_Shine.separate_chroma()
        G_Chroma.angle = 0
        G_Chroma.radius = 0

        G_PostEffect = G_Scanlines:chain(G_Chroma)
    end
    if self._splashVideo:tell() >= 12 and self._splashVideo:isPlaying() then
        self._time = 11
        self._splashVideo:pause()
    end
end

function GameStateSplash.KeyPressed(self,key,scancode,isrepeat)
    -- if key == "escape" and self._isSplash == false then self._time = 24 end
end

function GameStateSplash.Dispose(self)
    self._splashVideo = nil 
    self._burgerSplash = nil 
    self._intro3IMG = nil 
end

function GameStateSplash.Draw(self)
    if self._time < 5 then
        love.graphics.draw(G_GameInformations.m_splashPicture)
    elseif self._splashVideo:tell() < 13 and self._splashVideo:isPlaying() then
        love.graphics.draw(self._splashVideo , (G_GameInformations:GetWidth() - self._splashVideo:getWidth())/2 , (G_GameInformations:GetHeight() - self._splashVideo:getHeight())/2)
    elseif self._time >= 11 and self._time < 20 then
        love.graphics.draw(self._burgerSplash,0,0,0,self._scx,self._scy)
    elseif self._time >= 20 and self._time < 24 then 
        love.graphics.draw(self._intro3IMG,0,0,0,self._scx,self._scy)
    else

        if love.mouse.getX() >= (love.graphics.getWidth()/2) - 128 and love.mouse.getX() <= (love.graphics.getWidth()/2) + 128
        and love.mouse.getY() > (love.graphics.getHeight()/2) - 300 and love.mouse.getY() <= (love.graphics.getHeight()/2) - 44 then
            love.graphics.setColor(100,200,100)
        end
        love.graphics.draw(self._frFlag , (love.graphics.getWidth()/2) - 128 , (love.graphics.getHeight()/2) - 300 , 0 , 0.5 , 0.5)

        love.graphics.setColor(255,255,255)

        if love.mouse.getX() >= (love.graphics.getWidth()/2) - 128 and love.mouse.getX() <= (love.graphics.getWidth()/2) + 128
        and love.mouse.getY() > (love.graphics.getHeight()/2) and love.mouse.getY() <= (love.graphics.getHeight()/2) + 256 then
                love.graphics.setColor(100,200,100)
        end
        love.graphics.draw(self._ukFlag , (love.graphics.getWidth()/2) - 128 , (love.graphics.getHeight()/2) , 0 , 0.5 , 0.5)

        love.graphics.setColor(255,255,255)
        -- G_GameStateManager:Push("GameStateMainMenu")
    end
end
