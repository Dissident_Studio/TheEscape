--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : GameStateChooseLanguage.lua
Description : GameState that will handle the main menu of the game
Date        : 14/10/2016

--]]

GameStateChooseLanguage = {}

GameStateChooseLanguage_mt = {}

GameStateChooseLanguage_mt.__index = GameStateChooseLanguage

G_GameChangingLevel = {}
G_GameChangingLevel._is = false
G_GameChangingLevel._level = "none"

function GameStateChooseLanguage.New()

    local newobj = {
        _teEditor = true;

        -- Indicates if the gamestate has been loaded
        _loaded = false;

        -- The name of the gamestate
        _name = "GameStateChooseLanguage";

        -- Store the time of the game state
        _time = 0;

    }

    setmetatable(newobj,GameStateChooseLanguage_mt)
    return newobj

end

function GameStateChooseLanguage.IsLoaded(self)
    return self._loaded
end

function GameStateChooseLanguage.GetName(self)
    return self._name
end

function GameStateChooseLanguage.Load(self)
    -- Change the loaded state of the gamestate
    self._loaded = true
    -- Change the loaded state of the gamestate

end

function GameStateChooseLanguage.MousePressed(self,x,y,button)
    if button == 1 then
        -- x,y = G_GameInformations:InvertTransformFromDisplay(x,y)
        G_GameStateManager:Push("GameStateGame")
    end
end

function GameStateChooseLanguage.Update(self,dt)

end

function GameStateChooseLanguage.KeyPressed(self,key,scancode,isrepeat)

end

function GameStateChooseLanguage.Draw(self)
end
