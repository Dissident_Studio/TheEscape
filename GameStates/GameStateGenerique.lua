--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : GameStateGenerique.lua
Description : GameState that will handle the main menu of the game
Date        : 14/10/2016

--]]

GameStateGenerique = {}

GameStateGenerique_mt = {}

GameStateGenerique_mt.__index = GameStateGenerique


function GameStateGenerique.New()

    local newobj = {
        _teEditor = true;

        -- Indicates if the gamestate has been loaded
        _loaded = false;

        -- The name of the gamestate
        _name = "GameStateGenerique";

        -- Store the time of the game state
        _time = 0;

        -- 
        _video = love.graphics.newVideo("Resources/TheEscape_Credits.ogv") ;

        -- 
        _state = 0
    }

    setmetatable(newobj,GameStateGenerique_mt)
    return newobj

end

function GameStateGenerique.IsLoaded(self)
    return self._loaded
end

function GameStateGenerique.GetName(self)
    return self._name
end

function GameStateGenerique.Load(self)
    -- Change the loaded state of the gamestate
    self._loaded = true
    -- Change the loaded state of the gamestate

    self._video:rewind()
    self._video:play()


    self._w = G_GameInformations.m_width
    self._h = G_GameInformations.m_height
    self._scx = G_GameInformations.m_width/1024
    self._scy = G_GameInformations.m_height/768

end

function GameStateGenerique.MousePressed(self,x,y,button)

end

function GameStateGenerique.Update(self,dt)

    -- self._video:play()

    if self._video:tell() > 100 or not self._video:isPlaying() then 
        self._state = 1
        -- self._video:rewind()
    end

end

function GameStateGenerique.KeyPressed(self,key,scancode,isrepeat)

end

function GameStateGenerique.Draw(self)

    if self._state == 0 then

        love.graphics.draw(self._video , (G_GameInformations.m_width - self._video:getWidth())/2 , (G_GameInformations.m_height - self._video:getHeight())/2 )
        if self._video:tell() < 3 and G_GameInformations._additionFromActivator == "YES" then 
            self._pickingText = G_GameInformations:GetText("Bienvenue dans votre laboratoire professeur !")
            tx,ty = G_GameInformations:TransformFromDisplay(256,512)
            tx = tx - (#self._pickingText*10)/2
            love.graphics.print(self._pickingText,tx,ty)
        end
    else
        self._state = 0 ; self._time = 0
        G_GameStateManager:Push("GameStateMainMenu")
        self._loaded = false
    end

end
