


EditorObjects = {}

EditorObjects_mt = {}

EditorObjects_mt.__index = EditorObjects


function EditorObjects.New()

    local newobj = {

        m_scrollX = 0 ;
        m_scrollY = 0 ;

        _levelTransitors = {} ;
        _activators = {} ;
        _animated = {} ;
        _doors = {} ;
        _items = {} ;
        _notes = {} ;
        _panels = {} ;
        _numberOfObjects = 0 ;


    }

    setmetatable(newobj , EditorObjects_mt)

    return newobj 

end 


function EditorObjects.Load(self , level)

    local hdle = love.filesystem.newFile("Data/".. level .. "/" .. level ..".teo")

    local json_data = G_Json.decode(tostring(hdle:read()))

    if json_data.level_transitors then
        for i , level_transitor in ipairs(json_data.level_transitors) do 
            -- Create a new level transitor
            table.insert(self._levelTransitors , LevelTransitor.New(level_transitor))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end

    if json_data.activators then
        for i , activator in ipairs(json_data.activators) do 
            -- Create a new level transitor
            table.insert(self._activators , Activator.New(activator))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end 

    if json_data.animated then
        for i , animated in ipairs(json_data.animated) do 
            -- Create a new level transitor
            table.insert(self._animated , Animated.New(animated))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end 

    if json_data.doors then
        for i , door in ipairs(json_data.doors) do 
            -- Create a new level transitor
            table.insert(self._doors , Door.New(door))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end 

    if json_data.items then
        for i , item in ipairs(json_data.items) do 
            -- Create a new level transitor
            table.insert(self._items , Item.New(item))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end 

    if json_data.notes then
        for i , note in ipairs(json_data.notes) do 
            -- Create a new level transitor
            table.insert(self._notes , Note.New(note))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end

    if json_data.panels then
        for i , panel in ipairs(json_data.panels) do 
            -- Create a new level transitor
            table.insert(self._panels , Panel.New(panel))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end 



end 

function EditorObjects.Scroll(self , scrollX , scrollY)

    self.m_scrollX = self.m_scrollX + scrollX 
    self.m_scrollY = self.m_scrollY + scrollY 

end

function EditorObjects.Draw(self)


    for i,v in ipairs(self._items) do v:DrawEditor(self.m_scrollX , self.m_scrollY) end
    for i,v in ipairs(self._activators) do v:DrawEditor(self.m_scrollX , self.m_scrollY) end
    for i,v in ipairs(self._doors) do v:DrawEditor(self.m_scrollX , self.m_scrollY) end
    for i,v in ipairs(self._levelTransitors) do v:DrawEditor(self.m_scrollX , self.m_scrollY) end
    for i,v in ipairs(self._animated) do v:DrawEditor(self.m_scrollX , self.m_scrollY) end
    for i,v in ipairs(self._panels) do v:DrawEditor(self.m_scrollX , self.m_scrollY) end
    for i,v in ipairs(self._notes) do v:DrawEditor(self.m_scrollX , self.m_scrollY) end


end 