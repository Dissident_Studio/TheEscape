


EditorMap = {}

EditorMap_mt = {}

EditorMap_mt.__index = EditorMap 


function EditorMap.New()


    local newobj = {

        m_roomsPictures = {} ;

        m_values = {} ; 

        m_music = nil ;

        m_scrollX = 0 ;
        m_scrollY = 0 ;

    }

    -- Loading all the rooms pictures
    for i = 0 , G_GameInformations:GetNumberOfRooms() do
        table.insert(newobj.m_roomsPictures,love.graphics.newImage("Resources/Pics/Rooms/Room_" .. i .. ".png"))
    end

    setmetatable(newobj , EditorMap_mt)

    return newobj 

end 

function EditorMap.Load(self , level)

    local hdle = love.filesystem.newFile("Data/".. level .. "/" .. level ..".tem")

    hdle:open("r")

    local json_data = G_Json.decode(tostring(hdle:read()))

    self.m_width = json_data.width 
    self.m_height = json_data.height

    local count = 1
    for y = 0 , self.m_height-1 do 
        self.m_values[y] = {}
        for x = 0 , self.m_width-1 do 
            self.m_values[y][x] = 0
            self.m_values[y][x] = json_data.map[count]
            count = count + 1
        end 
    end 

    self.m_x = json_data.entry_x 
    self.m_y = json_data.entry_y

    self.m_music = json_data.music

end

function EditorMap.Scroll(self , x , y )

    self.m_scrollX = self.m_scrollX + x 
    self.m_scrollY = self.m_scrollY + y 

end

function EditorMap.Draw(self)

    for y = 0 , self.m_height-1 do
        for x = 0 , self.m_width-1 do 
            love.graphics.draw(self.m_roomsPictures[self.m_values[y][x]+1] 
            , (x * 256) + self.m_scrollX 
            , (y * 256) + self.m_scrollY 
            , 0 
            , 0.5 
            , 0.5)
        end 
    end 

end