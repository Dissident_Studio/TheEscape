--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : GameInfo.lua
Description : Class representing all the informations of the game
Date        : 14/10/2016

--]]

GameInfo = {}

GameInfo_mt = {}

GameInfo_mt.__index = GameInfo

function GameInfo.New(pathtogame)

    -- This class will be created at the beginning of the game
    -- It will read all the informations of the Game.conf file

    newobj = {
        _location = "Wonderful value"
    }

    DS_DEBUG("Reading configuration file " , "GameInfo")

    if BUILD == "Release" then
        newobj._location = pathtogame .. "/../";
    else
        newobj._location = pathtogame .. "/"
    end

    local fle = File.New(newobj._location .. "Game.conf")
    fle:Load()

    newobj._numberOfRooms = tonumber(fle:GetValueAsString("NUMBER_OF_ROOMS"))
    newobj._windowWidth = fle:GetValueAsNumber("WINDOW_WIDTH")
    newobj._windowHeight = fle:GetValueAsNumber("WINDOW_HEIGHT")
    newobj._fullscreen = fle:GetValueAsBool("FULLSCREEN")
    newobj._splashPicture = love.graphics.newImage(newobj._picturesPath .. fle:GetValueAsString("SPLASH_PICTURE"))
    newobj._splash = fle:GetValueAsBool("SPLASH")
    newobj._display = fle:GetValueAsString("DISPLAY")
    newobj._displayTarget = tonumber(fle:GetValueAsString("TARGET"))


    hdle = io.open(love.filesystem.getAppdataDirectory() .. "/TheEscape.sav" , "r")
    if hdle == nil then
        hdle = io.open(love.filesystem.getAppdataDirectory() .. "/TheEscape.sav" , "w")
        hdle:write("SAVED=Level_1\n")
        for i=1,22 do
            hdle:write("Level_"..i.."=false\n")
        end
        io.close(hdle)
    end

    fle = File.New(love.filesystem.getAppdataDirectory() .. "/TheEscape.sav")
    fle:Load()

    newobj._savedLevel = fle:GetValueAsString("SAVED")
    newobj._secrets = {}
    for i=1,22 do
        newobj._secrets["Level_"..i] = fle:GetValueAsString("Level_"..i)
    end

    DS_DEBUG("Configuration file reading is a succes" , "GameInfo")


    setmetatable(newobj,GameInfo_mt)

    return newobj

end

function GameInfo.ConfigureWindow(self)
    if self._fullscreen == true then
        love.window.setMode(self._windowWidth,self._windowHeight,
        {fullscreen = "true",fullscreentype="desktop",resizable="false"})
    else
        love.window.setMode(self._windowWidth,self._windowHeight,
        {resizable="false"})
    end
    self._windowWidth = love.graphics.getWidth()
    self._windowHeight = love.graphics.getHeight()

    love.window.setTitle("The Escape v1.0")
    love.window.setIcon(self._splashPicture:getData())

end

function GameInfo.GetPathToGame(self)
    return self._location
end

function GameInfo.GetPathToLevels(self)
    return self._location .. self._levelsPath
end

function GameInfo.GetPathToPictures(self)
    return self._location .. self._picturesPath
end

function GameInfo.GetPathToSounds(self)
    return self._location .. self._soundsPath
end

function GameInfo.GetPathToMusics(self)
    return self._location .. self._musicsPath
end

function GameInfo.GetPathToFonts(self)
    return self._location .. self._fontsPath
end

function GameInfo.GetNumberOfRooms(self)
    return self._numberOfRooms
end

function GameInfo.GetSplashPicture(self)
    return self._splashPicture
end

function GameInfo.GetVersion(self)
    return self._version
end

function GameInfo.GetDisplayType(self)
    return self._display
end

function GameInfo.GetLevelSaved(self)
    return self._savedLevel
end

function GameInfo.SaveLevel(self,level)

    local hdle = io.open(love.filesystem.getAppdataDirectory() .. "/TheEscape.sav","w+")
    hdle:write("SAVED=" .. level .. "\n")

    for i=1,22 do
        -- body...
        hdle:write("Level_"..i.."="..self._secrets["Level_"..i] .. "\n")
    end

    io.close(hdle)

end

function GameInfo.AddSecret(self,level)

    self._secrets[level] = "true"

end

function GameInfo.CheckSecret(self)

    for i=1,22 do
        -- body...
        if self._secrets["Level_"..i] == "false" or self._secrets["Level_"..i] == "False" then
            return false
        end
    end

    return true

end

function GameInfo.CountSecret(self)
        count = 0
        for i=1,22 do
        -- body...
        if self._secrets["Level_"..i] == "true" or self._secrets["Level_"..i] == "True" then
            count = count + 1
        end
    end

    return count

end


function GameInfo.TransformFromDisplay(self,x,y)
    if self._display == "CENTERED" then
        x = x + (self._windowWidth/2)
        y = y + (self._windowHeight/2)
        x = x - (self._displayTarget/2)
        y = y - (self._displayTarget/2)
        return x,y
    elseif self._display == "LEFT-CENTERED" then
        if x > (self._windowWidth/2) then x = x - (self._windowWidth/2) end
        y = y + (self._windowHeight/2) - (self._displayTarget/2)
        return x,y
    elseif self._display == "RIGHT-CENTERED" then
        if x < (self._windowWidth/2) then x = x + (self._windowWidth/2) end
        y = y + (self._windowHeight/2) - (self._displayTarget/2)
        return x,y
    end
end

function GameInfo.InvertTransformFromDisplay(self,x,y)
    if self._display == "CENTERED" then
        x = x - (self._windowWidth/2)
        y = y - (self._windowHeight/2)
        x = x + (self._displayTarget/2)
        y = y + (self._displayTarget/2)
        return x,y
    end
end

function GameInfo.SetFullScreen(self)
    self._fullscreen = not self._fullscreen
    self:ConfigureWindow()
end

function GameInfo.Resize(self,w,h)
    self._windowWidth = w
    self._windowHeight = h
end

function GameInfo.GetWindowDimensions(self)
    return self._windowWidth,self._windowHeight
end
