--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : Activator.lua
Description : Class that represents an activator in the game
Date        : 19/10/2016

--]]

Activator = Object.New()

Activator_mt = {}

Activator_mt.__index = Activator

function Activator.New(data)

    -- local newobj = {
    --     _activators = {} ;
    --     _timePassed = 0;
    --     _totalTimePassed = 0;
    --     _panelConditions = {};
    --     _itemConditions = {};
    --     _activatorConditions = {};
    -- }

    -- DS_ASSERT(nil == hdle)

    -- local buffer = hdle:read("*line")

    -- while buffer ~= "}" do
    --     if buffer ~= "}" then
    --         if buffer == "ITEM_CONDITIONS{" then

    --             Misc_LoadBracketsDefinition(hdle,newobj._itemConditions)
    --         elseif buffer == "ACTIVATOR_CONDITIONS{" then

    --             Misc_LoadBracketsDefinition(hdle,newobj._activatorConditions)
    --         elseif buffer == "DOORS{" then
    --             newobj._doors = {}
    --             Misc_LoadBracketsDefinition(hdle,newobj._doors)
    --         elseif buffer == "ANIMATED{" then
    --             newobj._animated = {}
    --             Misc_LoadBracketsDefinition(hdle,newobj._animated)
    --         elseif buffer == "PANEL_CONDITIONS{" then
    --             -- newobj._panelConditions = {}
    --             Misc_LoadBracketsDefinition(hdle,newobj._panelConditions)
    --         else
    --             key,value = Misc_KeyValue(buffer)
    --             if key ~= nil and value ~= nil then
    --                 if key == "ID" then newobj._id = value end
    --                 if key == "PICTURE_NAME" then newobj._pictureName = value end
    --                 if key == "ROOM_X" then newobj._roomX = tonumber(value) end
    --                 if key == "ROOM_Y" then newobj._roomY = tonumber(value) end
    --                 if key == "GRID_X" then newobj._gridX = tonumber(value) end
    --                 if key == "GRID_Y" then newobj._gridY = tonumber(value) end
    --                 if key == "TIMER" then newobj._timer = tonumber(value) end
    --             end
    --         end
    --     end
    --     buffer = hdle:read("*line")
    -- end

    -- newobj._loadedOnPicture = love.graphics.newImage("Resources/Pics/Activators/"..newobj._pictureName.."_On.png")
    -- newobj._loadedOffPicture = love.graphics.newImage("Resources/Pics/Activators/"..newobj._pictureName.."_Off.png")

    -- newobj._state = "Off"
    -- newobj._soundOff = love.audio.newSource("Resources/Sounds/Sounds/Activator_Off.wav")
    -- newobj._soundOn = love.audio.newSource("Resources/Sounds/Sounds/Activator_On.wav")
    -- newobj._soundOn:setVolume(0.05)
    -- newobj._soundOff:setVolume(0.05)

    -- newobj._timerSound = love.audio.newSource("Resources/Sounds/Sounds/Tick.wav")
    -- newobj._timerSound:setVolume(0.8)

    local newobj = Object.New(data)

    newobj._timer = data.timer
    newobj._pictureName = data.picture_name

    newobj._loadedOnPicture = love.graphics.newImage("Resources/Pics/Activators/"..newobj._pictureName.."_On.png")
    newobj._loadedOffPicture = love.graphics.newImage("Resources/Pics/Activators/"..newobj._pictureName.."_Off.png")

    newobj._state = "Off"
    if love.getVersion() > 10 then 
        newobj._soundOff = love.audio.newSource("Resources/Sounds/Sounds/Activator_Off.wav", "static")
        newobj._soundOn = love.audio.newSource("Resources/Sounds/Sounds/Activator_On.wav", "static")
    else 
        newobj._soundOff = love.audio.newSource("Resources/Sounds/Sounds/Activator_Off.wav") 
        newobj._soundOn = love.audio.newSource("Resources/Sounds/Sounds/Activator_On.wav") 
    end
    newobj._soundOn:setVolume(0.05)
    newobj._soundOff:setVolume(0.05)

    if love.getVersion() > 10 then 
        newobj._timerSound = love.audio.newSource("Resources/Sounds/Sounds/Tick.wav", "static")
    else 
        newobj._timerSound = love.audio.newSource("Resources/Sounds/Sounds/Tick.wav", "static") 
    end
    newobj._timerSound:setVolume(0.8)

    newobj._activatorConditions = (data.activator_conditions or {})
    newobj._panelConditions = (data.panel_conditions or {})
    newobj._itemConditions = (data.item_conditions or {})
    newobj._doors = (data.doors or {})
    newobj._animated = (data.animated or {})

    setmetatable(newobj,Activator_mt)

    return newobj

end

function Activator.Dispose(self)

    self._loadedOffPicture = nil 
    self._loadedOnPicture = nil 
    self._soundOff = nil 
    self._soundOn = nil 
    self._timerSound = nil 

end

function Activator.Draw(self,gridx,gridy)

    -- We draw the activator if we are in the right grid case
    if gridx == self._gridX
    and gridy == self._gridY
    then
        if self._state == "Off" then
            love.graphics.draw(self._loadedOffPicture
            ,G_GameInformations:TransformFromDisplay(self._roomX,self._roomY))
        else
            love.graphics.draw(self._loadedOnPicture
            ,G_GameInformations:TransformFromDisplay(self._roomX,self._roomY))
        end
    end

    -- We increase the timer
    if self._timer ~= nil and self._state == "On" then
        if self._timePassed < 1 then
            self._timePassed = self._timePassed + love.timer.getDelta()
        elseif self._timePassed >= 1 then
            
            if self._totalTimePassed < self._timer then
                self._totalTimePassed = self._totalTimePassed + self._timePassed
                self._timePassed = 0
                self._timerSound:play()
            else
                self._state = "Off"
                self._soundOn:play()
                self:Activate(self._objectsManager)
                self._timePassed = 0
                self._totalTimePassed = 0
            end
        end
    end

    -- love.graphics.print(#self._panelConditions)

end

function Activator.Action(self,gx,gy,x,y,objectsmanager,map)
    self._map = map
    if gx ~= self._gridX and gy ~= self._gridY then return end

    -- Transforming object Coordinates
    ox,oy = G_GameInformations:TransformFromDisplay(self._roomX,self._roomY)

    -- Here we action the activator if we are in the good grid case and the mouse is on the object
    if x >= ox and x <= ox + self._loadedOnPicture:getWidth()
    and y >= oy and y <= oy + self._loadedOnPicture:getHeight()
    and gx == self._gridX and gy == self._gridY
    then
        -- We activate the activator
        if self._state == "On" then
            map._pickingText = G_GameInformations:GetText("C'est déjà activé !")
            map._pickingTime = 750
            return true
        else
            if self:IsActivable(objectsmanager) then
                self._state = "On"
                if self._pictureName:find("Scanner") then
                    G_GameInformations:SetLevelSaved("Level_1")
                    G_GameInformations:Save()
                    G_GameInformations._additionFromActivator = "YES"
                    map._musics[1]:stop()            
                    map:ChangeLevel("Level_1")
                    G_GameStateManager:Push("GameStateGenerique")
                end
                self:Activate(objectsmanager)
                self._soundOn:play()
                return true
            else
                map._pickingText = G_GameInformations:GetText("Il vous manque quelque chose pour activer ceci !")
                map._pickingTime = 750
                self._soundOff:play()
                return true
            end
        end
    end
    
    return false

end

function Activator.DrawEditor(self , scrollX , scrollY)


    love.graphics.draw(self._loadedOnPicture 
    , (self._gridX * 256) + (self._roomX /2) + scrollX
    , (self._gridY * 256) + (self._roomY /2) + scrollY
    , 0
    , 0.5
    , 0.5 )

end

function Activator.Activate(self,objectsmanager)
    -- This function activates all the objects from which the activator is linked
    -- Activate the doors
    self._objectsManager = objectsmanager
    for i,v in ipairs(self._doors) do
        for j,w in ipairs(objectsmanager._doors) do
            if w._id == v then w:Activate() end
        end
    end

    -- Activate the activators
    -- for i,v in ipairs(self._activators) do
    --     -- body...
    --     for j,w in ipairs(objectsmanager._activators) do
    --         -- body...
    --         if w._id == v then w:Activate() end
    --     end
    -- end

    -- Activate animated
    for i,v in ipairs(self._animated) do
        for j,w in ipairs(objectsmanager._animated) do
            if w._id == v then w:Activate() end
        end
    end

    -- if there is a timer we set the time passed to 0
    if self._timer ~= nil then
        self._timePassed = 0
        self._totalTimePassed = 0
    end

end

function Activator.IsActivable(self,objectsmanager)
    -- All the boolean to see if we can activate the activator
    local ItemConditionsOk = false
    local NumberOfItemConditionsVerified = 0
    local ActivatorConditionsOk = false
    local NumberOfActivatorConditionsVerified = 0
    local ActivatorConditionsOk = false
    local NumberOfPanelConditionsVerified = 0

    -- Test for activation problems
    ItemConditionsOk = false
    NumberOfItemConditionsVerified = 0
    ActivatorConditionsOk = false
    NumberOfActivatorConditionsVerified = 0
    ActivatorConditionsOk = false
    NumberOfPanelConditionsVerified = 0

    if self._itemConditions then 
        for n,itemcondition in ipairs(self._itemConditions) do
            -- body...
            for i,item in ipairs(objectsmanager._items) do
                if item._state ~= "Dropped" and item._id == itemcondition then
                    item._state = "Consumed"
                    NumberOfItemConditionsVerified = NumberOfItemConditionsVerified +1
                end
            end
        end
    end

    ItemConditionsOk = false
    if NumberOfItemConditionsVerified >= #self._itemConditions or #self._itemConditions <= 0 then
        ItemConditionsOk = true
    end

    if self._activatorConditions then 
        for n,activatorcondition in ipairs(self._activatorConditions) do
            -- body...
            for i,activator in ipairs(objectsmanager._activators) do
                if activator._state == "On" and activator._id == activatorcondition then
                    NumberOfActivatorConditionsVerified = NumberOfActivatorConditionsVerified +1
                end
            end
        end
    end

    ActivatorConditionsOk = false
    if NumberOfActivatorConditionsVerified >= #self._activatorConditions or #self._activatorConditions <= 0 then
        ActivatorConditionsOk = true
    end

    if self._panelConditions then 
        for n,panelcondition in ipairs(self._panelConditions) do
            -- body...
            for i,panel in ipairs(objectsmanager._panels) do
                if panel._state == "On" and panel._id == panelcondition then
                    NumberOfPanelConditionsVerified = NumberOfPanelConditionsVerified +1
                end
            end
        end
    end

    PanelConditionsOk = false
    if (NumberOfPanelConditionsVerified >= #self._panelConditions) or (#self._panelConditions <= 0) then
        PanelConditionsOk = true
    end

    if (ActivatorConditionsOk == true) and (ItemConditionsOk == true) and (PanelConditionsOk == true) then
        return true
    else 
        return false 
    end

    return false

end

function Activator.Save(self,hdle)

    hdle:write("ACTIVATOR{\n")
    hdle:write("ID="..self._id.."\n")
    hdle:write("PICTURE_NAME="..self._pictureName.."\n")
    hdle:write("GRID_X="..self._gridX.."\n")
    hdle:write("GRID_Y="..self._gridY.."\n")
    hdle:write("ROOM_X"..self._roomX.."\n")
    hdle:write("ROOM_Y"..self._roomY.."\n")

    hdle:write("ITEM_CONDITIONS{")
    for i,v in ipairs(self._itemConditions) do
        -- body...
        hdle:write(v.."\n")
    end
    hdle:write("}\n")

    hdle:write("ACTIVATOR_CONDITIONS{")
    for i,v in ipairs(self._activatorConditions) do
        -- body...
        hdle:write(v.."\n")
    end
    hdle:write("}\n")

    hdle:write("DOORS{")
    for i,v in ipairs(self._doors) do
        -- body...
        hdle:write(v.."\n")
    end
    hdle:write("}\n")

    hdle:write("ANIMATED{")
    for i,v in ipairs(self._animated) do
        -- body...
        hdle:write(v.."\n")
    end
    hdle:write("}\n")

    hdle:write("}\n")

end
