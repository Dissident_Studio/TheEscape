--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : Panel.lua
Description : Class that represents a panel object in the game
Date        : 04/12/2016

--]]

--This is the snippet for the lvl definitions


Panel = Object.New()

Panel_mt = {}

Panel_mt.__index = Panel

function Panel.New(data)

    -- local newobj = {
    --     _state = "Off" ;
    --     _text = "";
    -- }

    -- DS_ASSERT(nil == hdle)

    -- setmetatable(newobj,Panel_mt)

    -- -- Reading properties of the item
    -- local buffer = hdle:read("*line")
    -- while buffer ~= "}" do
    --     if not Misc_IsEmpty(buffer) and not Misc_IsComment(buffer) and buffer ~= "}"then
    --         key,value = Misc_KeyValue(buffer)
    --         if key == "ID" then newobj._id = value end
    --         if key == "GRID_X" then newobj._gridX = tonumber(value) end
    --         if key == "GRID_Y" then newobj._gridY = tonumber(value) end
    --         if key == "ROOM_X" then newobj._roomX = tonumber(value) end
    --         if key == "ROOM_Y" then newobj._roomY = tonumber(value) end
    --         if key == "CODE" then newobj._code = value end
    --     end
    --     buffer = hdle:read("*line")
    -- end

    -- newobj._state = "Off"
    -- newobj._onPicture = love.graphics.newImage("Resources/Pics/Panels/Panel_On.png")
    -- newobj._offPicture = love.graphics.newImage("Resources/Pics/Panels/Panel_Off.png")
    -- newobj._onPictureShown = love.graphics.newImage("Resources/Pics/Panels/Panel_Shown_On.png")
    -- newobj._offPictureShown = love.graphics.newImage("Resources/Pics/Panels/Panel_Shown_Off.png")
    -- newobj._shown = false
    -- newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/Activator_On.wav")
    -- newobj._sound:setVolume(0.3)

    -- return newobj

    local newobj = Object.New(data)

    newobj._state = "Off" ; 

    newobj._text = "" ;

    newobj._code = data.code ;

    -- Load resources

    newobj._onPicture = love.graphics.newImage("Resources/Pics/Panels/Panel_On.png") 
    newobj._offPicture = love.graphics.newImage("Resources/Pics/Panels/Panel_Off.png") 
    newobj._offPictureShown = love.graphics.newImage("Resources/Pics/Panels/Panel_Shown_Off.png")
    newobj._onPictureShown = love.graphics.newImage("Resources/Pics/Panels/Panel_Shown_On.png")
    newobj._shown = false
    if love.getVersion() > 10 then
        newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/Activator_On.wav", "static")
    else 
        newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/Activator_On.wav")
    end
    newobj._sound:setVolume(0.3)

    -- 

    setmetatable(newobj , Panel_mt)

    return newobj

end

function Panel.Dispose(self)

    self._onPicture = nil 
    self._offPicture = nil 
    self._onPictureShown = nil 
    self._offPictureShown = nil 
    self._sound = nil 

end

function Panel.Update(self,dt)
end

function Panel.Draw(self,gridx,gridy,map)

    -- We draw the item if we are in the right grid case
    if gridx == self._gridX
    and gridy == self._gridY
    then
        if self._shown == true then
            drawx,drawy = G_GameInformations:TransformFromDisplay(128,128)
            if self._state == "On" then
                love.graphics.draw(self._onPictureShown,drawx,drawy)
            else
                love.graphics.draw(self._offPictureShown,drawx,drawy)
            end
            love.graphics.print(self._text,drawx+(6*14)+6,drawy+(6*14))
        else
            drawx,drawy = G_GameInformations:TransformFromDisplay(self._roomX,self._roomY)
            if self._state == "On" then
                love.graphics.draw(self._onPicture,drawx,drawy)
            else
                love.graphics.draw(self._offPicture,drawx,drawy)
            end
        end
    end
end

function Panel.Action(self,gx,gy,x,y,map)

    if gx ~= self._gridX and gy ~= self._gridY then return end

    -- Transforming object Coordinates
    ox,oy = G_GameInformations:TransformFromDisplay(self._roomX,self._roomY)

    if gx == self._gridX and gy == self._gridY and self._shown == true then
        self._shown = false
        -- self._text = ""
        return
    end

    -- Here we action the item if we are in the good grid case and the mouse is on the object
    if x >= ox and x <= ox + self._onPicture:getWidth()
    and y >= oy and y <= oy + self._onPicture:getHeight()
    and gx == self._gridX and gy == self._gridY
    then
        if self._shown == true then
            self._shown = false
            -- self._text = ""
        else
            self._shown = true
        end
    end

end

function Panel.Backspace(self)

    self._text = self._text:sub(0,#self._text-1)
    love.timer.sleep(0.1)
    
end

function Panel.Activate(self)

    self._state = "On"
    self._sound:play()

end

function Panel.TextInput(self,t)

    if self._shown == true then
        if #self._text < 6 and tonumber(t) then
            self._text = self._text .. t
        end

        if self._text == self._code then
            self:Activate()
        end
    end

end


function Panel.DrawEditor(self , scrollX , scrollY)


    love.graphics.draw(self._onPicture 
    , (self._gridX * 256) + (self._roomX /2) + scrollX
    , (self._gridY * 256) + (self._roomY /2) + scrollY
    , 0
    , 0.5
    , 0.5 )

end
