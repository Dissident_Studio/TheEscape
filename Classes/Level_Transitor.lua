--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : LevelTransitor.lua
Description : Class that represents a level transitor in the game
Date        : 21/10/2016

--]]

-- '.source.go':
--   'Level Transitor Definition':
--     'prefix': 'telt'
--     'body': '
--     LEVELTRANSITOR{\n
--     ID=LVL_TR_0$1\n
--     PICTURE_NAME=$2\n
--     SOUND_NAME=$3\n
--     LEVEL=$4\n
--     GRID_X=$5\n
--     GRID_Y=$6\n
--     ROOM_X=$7\n
--     ROOM_Y=$8\n
--     }'
--     'description': 'Declares a level transitor for the escape level system'

LevelTransitor = Object.New()

LevelTransitor_mt = {}

LevelTransitor_mt.__index = LevelTransitor

function LevelTransitor.New(data)

    -- local newobj = {

    -- }

    -- DS_ASSERT(nil == hdle)

    -- buffer = hdle:read("*line")

    -- while buffer ~= "}" do
    --     if buffer ~= "}" and not Misc_IsComment(buffer) and not Misc_IsEmpty(buffer) then
    --         key,value = Misc_KeyValue(buffer)
    --         if key ~= nil and value ~= nil then
    --             if key == "ID" then newobj._id = value end
    --             if key == "PICTURE_NAME" then newobj._pictureName = value end
    --             if key == "SOUND_NAME" then newobj._soundName = value end
    --             if key == "LEVEL" then newobj._toLevel = value end
    --             if key == "GRID_X" then newobj._gridX = tonumber(value) end
    --             if key == "GRID_Y" then newobj._gridY = tonumber(value) end
    --             if key == "ROOM_X" then newobj._roomX = tonumber(value) end
    --             if key == "ROOM_Y" then newobj._roomY = tonumber(value) end
    --         end
    --     end
    --     buffer = hdle:read("*line")
    -- end

    -- newobj._loadedPicture = love.graphics.newImage("Resources/Pics/LevelTransitors/"..newobj._pictureName..".png")
    -- newobj._loadedSound = love.audio.newSource("Resources/Sounds/Sounds/"..newobj._soundName .. ".wav")

    -- setmetatable(newobj,LevelTransitor_mt)

    -- return newobj

    -- NEW

    local newobj = Object.New(data)

    -- The picture name of the lt
    newobj._pictureName = data.picture_name ;

    -- The sound name
    newobj._soundName = data.sound_name ;

    -- The level to which the lt leads
    newobj._level = data.level ;

    newobj._loadedPicture = love.graphics.newImage("Resources/Pics/LevelTransitors/" .. newobj._pictureName .. ".png")
    if love.getVersion() > 10 then 
        newobj._loadedSound = love.audio.newSource("Resources/Sounds/Sounds/"..newobj._soundName .. ".wav", "static")
    else
        newobj._loadedSound = love.audio.newSource("Resources/Sounds/Sounds/"..newobj._soundName .. ".wav")
    end

    newobj._toLevel = data.level

    setmetatable(newobj , LevelTransitor_mt)

    return newobj
    -- NEW

end

function LevelTransitor.Dispose(self)

    self._loadedPicture = nil 
    self._loadedSound = nil 

end

function LevelTransitor.Update(self,dt)

end

function LevelTransitor.Action(self,gx,gy,x,y,map)


    if gx ~= self._gridX and gy ~= self._gridY then self._pickingText = "RETURNING" ; self._pickingTime = 750 ; return end

    tx,ty = G_GameInformations:TransformFromDisplay(self._roomX,self._roomY)
    if x >= tx and x <= tx + self._loadedPicture:getWidth() and
    y >= ty and y <= ty + self._loadedPicture:getHeight() and
    gx == self._gridX and gy == self._gridY then
        -- if map._level == "Level_1" then G_GameStateManager:Push("GameStateGenerique") end

        if map._level == "Level_Secret_3" then
            love.system.openURL("www.dissidentstudio.fr/theescape-congratulations")
            love.window.minimize()
            self._loadedSound:play()
            G_GameInformations:SetLevelSaved(self._toLevel)
            G_GameInformations:Save()
            map:ChangeLevel("Level_1")
            map._x = 1
            map._y = 2
        end

        if self._id == "TRANSITOR_EXIT" then 
            G_GameStateManager:Push("GameStateGenerique")
        end

        self._loadedSound:play()
        G_GameInformations:SetLevelSaved(self._toLevel)
        G_GameInformations:Save()
        map:ChangeLevel(self._toLevel)
    end
end

function LevelTransitor.Draw(self,gridx,gridy)

    if gridx == self._gridX and gridy == self._gridY then
        love.graphics.draw(self._loadedPicture
        ,G_GameInformations:TransformFromDisplay(self._roomX,self._roomY))
    end

end

function LevelTransitor.DrawEditor(self , scrollX , scrollY)


    love.graphics.draw(self._loadedPicture 
    , (self._gridX * 256) + (self._roomX /2) + scrollX
    , (self._gridY * 256) + (self._roomY /2) + scrollY
    , 0
    , 0.5
    , 0.5 )

end

