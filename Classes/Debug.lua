--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : Debug.lua
Description : Debugger for the escape
Date        : 14/10/2016

--]]

function DS_DEBUG(debug,classname)

    if DEBUG == true then
        if classname ~= nil then
            print("[The Escape] | [" .. classname .. "] -> " .. debug)
        else
            print("[The Escape] -> " .. debug)
        end
    end
end

function DS_ASSERT(assertion)

    if assertion == true then
        error("[ASSERTION FAILED] -> Printing Stack Trace :\n" .. debug.traceback("Stack Trace"))
    end

end

function DS_ERROR(msg)

    error("[The Escape] -> ERROR : " .. msg)

end
