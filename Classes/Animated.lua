--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : Animated.lua
Description : Class that represents an animated in the game
Date        : 19/10/2016

--]]

Animated = Object.New()

Animated_mt = {}

Animated_mt.__index = Animated

function Animated.New(data)

    -- local newobj = {

    -- }

    -- DS_ASSERT(nil == hdle)

    -- local buffer = hdle:read("*line")

    -- while buffer ~= "}" do
    --     if buffer ~= "}" then
    --         key,value = Misc_KeyValue(buffer)
    --         if key ~= nil and value ~= nil then
    --             if key == "ID" then newobj._id = value end
    --             if key == "PICTURE_ON_NAME" then newobj._pictureOnName = value end
    --             if key == "PICTURE_OFF_NAME" then newobj._pictureOffName = value end
    --             if key == "SOUND" then newobj._soundName = value end
    --             if key == "GRID_X" then newobj._gridX = tonumber(value) end
    --             if key == "GRID_Y" then newobj._gridY = tonumber(value) end
    --             if key == "ROOM_X" then newobj._roomX = tonumber(value) end
    --             if key == "ROOM_Y" then newobj._roomY = tonumber(value) end
    --             if key == "STATE" then newobj._state = value end
    --         end
    --     end
    --     buffer = hdle:read("*line")
    -- end

    -- newobj._loadedOnPicture = love.graphics.newImage("Resources/Pics/Animated/"..newobj._pictureOnName.."_On.png")
    -- if newobj._pictureOffName ~= nil then
    --     newobj._loadedOffPicture = love.graphics.newImage("Resources/Pics/Animated/"..newobj._pictureOffName.."_Off.png")
    -- end
    -- if newobj._soundName ~= nil then newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/"..newobj._soundName..".wav")
    -- else
    --     newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/"..newobj._pictureOnName .. ".wav")
    -- end
    -- newobj._sound:setVolume(0.1)

    -- newobj._animation = {}
    -- newobj._animation._state = 0
    -- newobj._animation._time = 0

    -- newobj._animation._frames = newobj._loadedOnPicture:getWidth()/newobj._loadedOnPicture:getHeight()

    local newobj = Object.New(data)

    newobj._pictureOnName = data.picture_on_name 
    newobj._pictureOffName = data.picture_off_name 
    newobj._soundName = data.sound
    newobj._state = data.state


    newobj._loadedOnPicture = love.graphics.newImage("Resources/Pics/Animated/"..newobj._pictureOnName.."_On.png")
    if newobj._pictureOffName ~= nil then
        newobj._loadedOffPicture = love.graphics.newImage("Resources/Pics/Animated/"..newobj._pictureOffName.."_Off.png")
    end
    if newobj._soundName ~= nil then 
        if love.getVersion() > 10 then 
            newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/"..newobj._soundName..".wav", "static")
        else 
            newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/"..newobj._soundName..".wav")
        end
    else
        if love.getVersion() > 10 then 
            newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/"..newobj._pictureOnName .. ".wav", "static")
        else
            newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/"..newobj._pictureOnName .. ".wav")
        end
    end
    newobj._sound:setVolume(0.1)

    newobj._animation = {}
    newobj._animation._state = 0
    newobj._animation._time = 0

    newobj._animation._frames = newobj._loadedOnPicture:getWidth()/newobj._loadedOnPicture:getHeight()


    setmetatable(newobj,Animated_mt)

    return newobj

end

function Animated.Dispose(self)

    self._loadedOnPicture = nil 
    self._loadedOffPicture = nil 
    self._sound = nil 

end

function Animated.Activate(self)
    if string.lower(self._state) == "off" then 
        self._state = "on"
    else 
        self._state = "off"   
    end

end

function Animated.Update(self,dt)

    if string.lower(self._state) == "on" then
        if self._animation._time > 0.1 then
            if self._animation._state+1 < self._animation._frames then
                self._animation._state = self._animation._state + 1
                self._animation._time = 0
            else
                self._animation._state = 0
                self._animation._time = 0
            end
        else
            self._animation._time = self._animation._time + love.timer.getDelta()
        end
    end

end

function Animated.Draw(self,gridx,gridy)

    if gridx == self._gridX and gridy == self._gridY then
        if string.lower(self._state) == "on" then
            love.graphics.draw(self._loadedOnPicture
            ,love.graphics.newQuad(self._animation._state*self._loadedOnPicture:getHeight(),0
            ,self._loadedOnPicture:getHeight(),self._loadedOnPicture:getHeight()
            ,self._loadedOnPicture:getDimensions())
            ,G_GameInformations:TransformFromDisplay(self._roomX,self._roomY))
            self._sound:play()
        elseif string.lower(self._state) == "off" and self._pictureOffName ~= nil then
            love.graphics.draw(self._loadedOffPicture,G_GameInformations:TransformFromDisplay(self._roomX,self._roomY))
        end

    else
        self._sound:stop()
    end

end

function Animated.DrawEditor(self , scrollX , scrollY)


    love.graphics.draw(self._loadedOnPicture 
    , (self._gridX * 256) + (self._roomX /2) + scrollX
    , (self._gridY * 256) + (self._roomY /2) + scrollY
    , 0
    , 0.5
    , 0.5 )

end
