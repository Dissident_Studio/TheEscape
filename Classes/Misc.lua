--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - Lua Facilities
Author      : Yannis Beaux (Kranck)
File        : Misc.lua
Description : Useful functions for lua and LOVE2D
Date        : 09/10/2016

--]]

function Misc_GetPathToFile(argpath , file , formatantislash)

    absolute = args

    completepath = argpath .. "/" .. file

    return completepath
end

function Misc_GenerateRandomString(nbCharacter)

    charset = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q'
    ,'R','S','T','U','V','W','X','Y','Z'}

    ret = ""
    for i = 0 , nbCharacter do
        if math.random(1,2) == 1 then
            if math.random(1,2) then
                ret = ret .. string.lower(charset[math.random(1,26)])
            else
                ret = ret .. charset[math.random(1,26)]
            end
        else
            ret = ret .. math.random(0,9)
        end
    end

    return ret

end

function Misc_IsEmpty(line)

    -- Returns true if the line is empty
    if line:len() == 0 then
        return true
    else
        return false
    end

end

function Misc_IsComment(line)

    -- This function returns true if a line is a comment
    -- A comment is a line starting with # or ; or --
    if line:find("#") == 1 or line:find(";") == 1 then
        return true
    else
        return false
    end

end

function Misc_LoadBracketsDefinition(hdle,tab)
    DS_ASSERT(nil == hdle)

    buffer = hdle:read("*line")

    while buffer ~= "}" do
        if buffer ~= "}" and not Misc_IsEmpty(buffer) and not Misc_IsComment(buffer) then
            table.insert(tab,buffer)
        end
        buffer = hdle:read("*line")
    end

end


function Misc_KeyValue(line)

    -- This function returns the key and he value in a line like KEY=Value

    occurence = line:find("=")
    if occurence ~= nil then

        key = line:sub(1,occurence-1)
        value = line:sub(occurence+1)
        return key,value

    else return nil,nil end

end

function Misc_StringToBool(line)
    line = string.lower(line)
    if line == "true" then
        return true
    else
        return false
    end
end
