--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : Item.lua
Description : Class that represents a pickable item in the game
Date        : 17/10/2016

--]]

--This is the snippet for the lvl definitions


Item = Object.New()

Item_mt = {}

Item_mt.__index = Item

function Item.New(data)

    local newobj = Object.New(data)

    -- local newobj = {
    --     _state = "Dropped" ;
    -- }

    -- DS_ASSERT(nil == hdle)

    -- setmetatable(newobj,Item_mt)

    -- -- Reading properties of the item
    -- local buffer = hdle:read("*line")
    -- while buffer ~= "}" do
    --     if not Misc_IsEmpty(buffer) and not Misc_IsComment(buffer) and buffer ~= "}"then
    --         key,value = Misc_KeyValue(buffer)
    --         if key == "ID" then newobj._id = value end
    --         if key == "PICTURE_NAME" then newobj._pictureName = value end
    --         if key == "GRID_X" then newobj._gridX = tonumber(value) end
    --         if key == "GRID_Y" then newobj._gridY = tonumber(value) end
    --         if key == "ROOM_X" then newobj._roomX = tonumber(value) end
    --         if key == "ROOM_Y" then newobj._roomY = tonumber(value) end
    --         if key == "PICKABLE" then newobj._pickable = Misc_StringToBool(value) end
    --     end
    --     buffer = hdle:read("*line")
    -- end

    -- newobj._state = "Dropped"
    -- newobj._loadedPicture = love.graphics.newImage("Resources/Pics/Items/" .. newobj._pictureName .. ".png")
    -- newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/Pick_Up.wav")
    -- newobj._sound:setVolume(0.2)

    -- return newobj

    newobj._pictureName = data.picture_name ;
    newobj._pickable = data.pickable ;

    newobj._state = "Dropped"
    newobj._loadedPicture = love.graphics.newImage("Resources/Pics/Items/" .. newobj._pictureName .. ".png")
    if love.getVersion() > 10 then 
        newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/Pick_Up.wav", "static")
    else 
        newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/Pick_Up.wav")
    end
    newobj._sound:setVolume(0.2)

    setmetatable(newobj , Item_mt)

    return newobj

end

function Item.Dispose(self)

    self._loadedPicture = nil 
    self._sound = nil 

end

function Item.Update(self,dt)
end

function Item.Draw(self,gridx,gridy,map)

    -- We draw the item if we are in the right grid case
    if gridx == self._gridX
    and gridy == self._gridY
    and self._state == "Dropped"
    then
        if self._pictureName == "Secret" and G_GameInformations.m_secrets[map._level] == true then
            return
        else 
            local drawx , drawy = G_GameInformations:TransformFromDisplay(self._roomX,self._roomY)
            if self._pictureName == "TutoRoomOne" then 

                if G_Lang == "fr" then 
                    love.graphics.printf(
                        "Clic droit pour \n prendre un objet\n\n\n Clic gauche pour bouger" 
                        , drawx + 180
                        , drawy + 140
                        , 1024)
                else 
                    love.graphics.printf(
                        "Right click to \n take an item\n\n\n             Left click to move " 
                        , drawx + 200
                        , drawy + 140
                        , 1024 )
                end

            elseif self._pictureName == "TutoRoomTwo" then 

                if G_Lang == "fr" then 
                    love.graphics.printf(
                        "Clic droit pour activer" 
                        , drawx + 120
                        , drawy + 140
                        , 1024)
                else 
                    love.graphics.printf(
                        "Right click to activate" 
                        , drawx + 120
                        , drawy + 140
                        , 1024 )
                end
            elseif self._pictureName == "TutoRoomCode" then 

                if G_Lang == "fr" then 
                    love.graphics.printf(
                        "Saisissez le code au clavier" 
                        , drawx + 120
                        , drawy + 140
                        , 1024)
                else 
                    love.graphics.printf(
                        "Use the keyboard to type \n the code" 
                        , drawx + 120
                        , drawy + 120
                        , 1024 )
                end

            else 
                love.graphics.draw(self._loadedPicture
                ,G_GameInformations:TransformFromDisplay(self._roomX,self._roomY))
            end
            
        end
    end
end



function Item.DrawEditor(self , scrollX , scrollY)


    love.graphics.draw(self._loadedPicture 
    , (self._gridX * 256) + (self._roomX /2) + scrollX
    , (self._gridY * 256) + (self._roomY /2) + scrollY
    , 0
    , 0.5
    , 0.5 )

end

function Item.Action(self,gx,gy,x,y,map)

    if gx ~= self._gridX and gy ~= self._gy then return end

    -- Transforming object Coordinates
    ox,oy = G_GameInformations:TransformFromDisplay(self._roomX,self._roomY)

    -- Here we action the item if we are in the good grid case and the mouse is on the object
    if x >= ox and x <= ox + self._loadedPicture:getWidth()
    and y >= oy and y <= oy + self._loadedPicture:getHeight()
    then
        -- If the item is pickable we pick it
        if self._pickable and self._state == "Dropped" then
            self._state = "Picked"
            map._pickingTime = 3000;
            map._pickingText = G_GameInformations:GetText("Recupéré : ") .. G_GameInformations:GetText(self._pictureName)
            if self._pictureName == "Secret" then 
                G_GameInformations:AddSecret(map._level)
                if G_GameInformations:CheckSecret() then 
                    map._pickingText = G_GameInformations:GetText("Salles secretes découvertes ! Trouvez la porte violette ...")
                else
                    map._pickingText = G_GameInformations:GetText("Recupéré : ") .. " Secret " .. G_GameInformations:CountSecret() .. "/21"
                end
            end
            self._sound:play()
            return true
        end
        if self._pictureName == "Secret" then
            -- Ici nous avons récupéré un Secret
            return true
        end
    end

    return false

end

function Item.Save(self,hdle)

    hdle:write("ITEM{\n")
    hdle:write("ID="..self._id.."\n")
    hdle:write("PICTURE_NAME"..self._pictureName.."\n")
    hdle:write("GRID_X="..self._gridX.."\n")
    hdle:write("GRID_Y="..self._gridY.."\n")
    hdle:write("ROOM_X="..self._roomX.."\n")
    hdle:write("ROOM_Y="..self._roomY.."\n")
    hdle:write("PICKABLE="..self._pickable)
    hdle:write("}\n")

end
