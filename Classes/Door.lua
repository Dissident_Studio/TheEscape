--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : Door.lua
Description : Class that represents a door in the game
Date        : 19/10/2016

--]]

Door = Object.New()

Door_mt = {}

Door_mt.__index = Door

function Door.New(data)

    -- local newobj = {

    -- }

    -- DS_ASSERT(nil == hdle)

    -- local buffer = hdle:read("*line")

    -- while buffer ~= "}" do
    --     if buffer ~= "}" then
    --         key,value = Misc_KeyValue(buffer)
    --         if key ~= nil and value ~= nil then
    --             if key == "ID" then newobj._id = value end
    --             if key == "DIRECTION" then newobj._direction = value end
    --             if key == "GRID_X" then newobj._gridX = tonumber(value) end
    --             if key == "GRID_Y" then newobj._gridY = tonumber(value) end
    --             if key == "STATE" then newobj._state = value end
    --         end
    --     end
    --     buffer = hdle:read("*line")
    -- end



    local newobj = Object.New(data)

    newobj._state = data.state 

    newobj._direction = data.direction


    newobj._animation = {}
    newobj._animation._time = 0
    newobj._animation._state = 0

    newobj._loadedOpenedPicture = love.graphics.newImage("Resources/Pics/Doors/Door_" .. newobj._direction .. "_Opened.png")
    newobj._loadedClosedPicture = love.graphics.newImage("Resources/Pics/Doors/Door_" .. newobj._direction .. "_Closed.png")
    if newobj._direction ~= "Secret" then
        newobj._loadedOpeningPicture = love.graphics.newImage("Resources/Pics/Doors/Door_" .. newobj._direction .. "_Opening.png")
    end

    if love.getVersion() > 10 then 
        newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/Door.wav", "static")
    else
        newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/Door.wav")
    end
    newobj._sound:setVolume(0.2)

    setmetatable(newobj,Door_mt)

    return newobj

end

function Door.Dispose(self)

    self._loadedOpenedPicture = nil 
    self._loadedClosedPicture = nil 
    self._loadedOpeningPicture = nil 
    self._sound = nil 

end

function Door.Activate(self)

    if self._state == "Opened" then
        self._state = "Closing"
        self._animation._state = 3
        self._animation._time = 0
        self._sound:play()
    elseif self._state == "Closed" then
        self._state = "Opening"
        self._animation._state = 0
        self._animation._time = 0
        self._sound:play()
    elseif self._state == "Closing" then self._state = "Opening"
    elseif self._state == "Opening" then self._state = "Closing" end
end

function Door.Update(self,dt)
    if self._state == "Opening" then
        if self._animation._time > 0.1 then
            if self._animation._state < 3 then
                self._animation._state = self._animation._state + 1
                self._animation._time = 0
            else
                self._animation._state = 0
                self._animation._time = 0
                self._state = "Opened"
            end
        else
            self._animation._time = self._animation._time + love.timer.getDelta()
        end
    elseif self._state == "Closing" then
        if self._animation._time > 0.1 then
            if self._animation._state > 0 then
                self._animation._state = self._animation._state - 1
                self._animation._time = 0
            else
                self._animation._state = 0
                self._animation._time = 0
                self._state = "Closed"
            end
        else
            self._animation._time = self._animation._time + love.timer.getDelta()
        end
    end


end

function Door.Draw(self,gridx,gridy)

    if self._gridX == gridx and self._gridY == gridy then
        if self._state == "Opened" then
            love.graphics.draw(self._loadedOpenedPicture,G_GameInformations:TransformFromDisplay(0,0))
        elseif self._state == "Closed" then
            love.graphics.draw(self._loadedClosedPicture,G_GameInformations:TransformFromDisplay(0,0))
        else
            love.graphics.draw(self._loadedOpeningPicture,
            love.graphics.newQuad(self._animation._state*512,0,512,512,512*4,512)
            ,G_GameInformations:TransformFromDisplay(0,0))
        end
    end

end

function Door.DrawEditor(self , scrollX , scrollY)


    love.graphics.draw(self._loadedClosedPicture 
    , (self._gridX * 256) + (self._roomX /2) + scrollX
    , (self._gridY * 256) + (self._roomY /2) + scrollY
    , 0
    , 0.5
    , 0.5 )

end
