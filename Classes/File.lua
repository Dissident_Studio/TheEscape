--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : File.lua
Description : File parsing system
Date        : 14/10/2016

--]]

-- Requires
require "Classes/Misc"
-- Requires

File = {}

File_mt = {}

File_mt.__index = File

function File.New(absolutepath)

    local newobj = {

        -- The absolute path of the file
        _absolutepath = absolutepath ;

        -- All key values table
        _content = {} ;
    }

    setmetatable(newobj,File_mt)

    return newobj

end

function File.Load(self)

    hdle = io.open(self._absolutepath , "r")

    DS_ASSERT(nil == hdle)

    buffer = hdle:read("*line")

    while buffer ~= nil do

        if not Misc_IsEmpty(buffer) and not Misc_IsComment(buffer) then
            key,value = Misc_KeyValue(buffer)
            self._content[key] = value
        end
        buffer = hdle:read("*line")
    end

end

function File.GetValueAsString(self,key)
    return self._content[key]
end

function File.GetValueAsNumber(self,key)
    return tonumber(self._content[key])
end

function File.GetValueAsBool(self,key)

    if string.lower(self._content[key]) == "true" then
        return true
    else
        return false
    end

end
