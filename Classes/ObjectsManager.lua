--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : ObjectsManager.lua
Description : Class to manage the objects of a level
Date        : 17/10/2016

--]]

require "Classes/Object"

require "Classes/Item"
require "Classes/Activator"
require "Classes/Door"
require "Classes/Animated"
require "Classes/Level_Transitor"
require "Classes/Panel"
require "Classes/Note"

ObjectsManager = {}

ObjectsManager_mt = {}

ObjectsManager_mt.__index = ObjectsManager

function ObjectsManager.New()

    local newobj = {

        -- Here are stored all the objects of the level
        _objects = {} ;

        -- The number of objects
        _numberOfObjects = 0 ;

        -- The actual level of the game
        _level = "nil" ;

    }

    setmetatable(newobj,ObjectsManager_mt)

    return newobj

end

function ObjectsManager.Dispose(self)

    for i , v in ipairs(self._objects) do 
        v:Dispose()
        v = nil 
    end 

    self._objects = nil 

end

function ObjectsManager.Load(self , level)

    -- Initilizing the objects types
    self._level = level
    self._items = {}
    self._animated = {}
    self._activators = {}
    self._doors = {}
    self._levelTransitors = {}
    self._panels = {}
    self._notes = {}

    -- OLD
    -- DS_DEBUG("Loading level objects")

    -- -- Initilizing the objects types
    -- self._level = level
    -- self._items = {}
    -- self._animated = {}
    -- self._activators = {}
    -- self._doors = {}
    -- self._levelTransitors = {}
    -- self._panels = {}
    -- self._notes = {}

    -- -- Opening file
    -- local hdle = io.open(G_GameInformations:GetPathToGame() .. "Data/" .. level .. "/" .. level .. ".teo")

    -- DS_ASSERT(nil == hdle)

    -- buffer = hdle:read("*line")

    -- -- Reading all the items
    -- while buffer ~= nil do
    --     if not Misc_IsComment(buffer) and not Misc_IsEmpty(buffer) then
    --         if buffer == "ITEM{" then
    --             -- Creating new item
    --             table.insert(self._items,Item.New(hdle))
    --             self._numberOfObjects = self._numberOfObjects + 1
    --         elseif buffer == "ANIMATED{" then
    --             -- Creating an animated
    --             table.insert(self._animated,Animated.New(hdle))
    --             self._numberOfObjects = self._numberOfObjects + 1
    --         elseif buffer == "DOOR{" then
    --             -- Creating a door
    --             table.insert(self._doors,Door.New(hdle))
    --             self._numberOfObjects = self._numberOfObjects + 1
    --         elseif buffer == "ACTIVATOR{" then
    --             -- Creating activator
    --             table.insert(self._activators,Activator.New(hdle))
    --             self._numberOfObjects = self._numberOfObjects + 1
    --         elseif buffer == "PANEL{" then
    --             -- Creating a panel
    --             table.insert(self._panels,Panel.New(hdle))
    --             self._numberOfObjects = self._numberOfObjects + 1
    --         elseif buffer == "NOTE{" then 
    --             -- Creating a note
    --             table.insert(self._notes,Note.New(hdle))
    --             self._numberOfObjects = self._numberOfObjects + 1
    --         elseif buffer =="LEVELTRANSITOR{" then
    --             -- Creating a level transitor
    --             table.insert(self._levelTransitors,LevelTransitor.New(hdle))
    --             self._numberOfObjects = self._numberOfObjects + 1
    --         end
    --     end
    --     buffer = hdle:read("*line")
    -- end

    -- DS_DEBUG("Level Objects loaded !")

    -- OLD



    -- NEW

    local hdle = love.filesystem.newFile("Data/".. level .. "/" .. level ..".teo")

    local json_data = G_Json.decode(tostring(hdle:read()))

    if json_data.level_transitors then
        for i , level_transitor in ipairs(json_data.level_transitors) do 
            -- Create a new level transitor
            table.insert(self._levelTransitors , LevelTransitor.New(level_transitor))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end

    if json_data.activators then
        for i , activator in ipairs(json_data.activators) do 
            -- Create a new level transitor
            table.insert(self._activators , Activator.New(activator))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end 

    if json_data.animated then
        for i , animated in ipairs(json_data.animated) do 
            -- Create a new level transitor
            table.insert(self._animated , Animated.New(animated))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end 

    if json_data.doors then
        for i , door in ipairs(json_data.doors) do 
            -- Create a new level transitor
            table.insert(self._doors , Door.New(door))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end 

    if json_data.items then
        for i , item in ipairs(json_data.items) do 
            -- Create a new level transitor
            table.insert(self._items , Item.New(item))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end 

    if json_data.notes then
        for i , note in ipairs(json_data.notes) do 
            -- Create a new level transitor
            table.insert(self._notes , Note.New(note))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end

    if json_data.panels then
        for i , panel in ipairs(json_data.panels) do 
            -- Create a new level transitor
            table.insert(self._panels , Panel.New(panel))
            -- TODO: Put this in a function
            self._numberOfObjects = self._numberOfObjects + 1
        end
    end 


end

function ObjectsManager.IsADoorBlocking(self,gx,gy,direction)

    -- Testing for the secret door
    for i,v in ipairs(self._doors) do
        if gx == v._gridX and gy == v._gridY and v._direction == "Secret" and direction == "Down" and v._state == "Closed" then
            return true
        end
    end

    for i,v in ipairs(self._doors) do
        if gx == v._gridX and gy == v._gridY
        and v._direction == direction and v._state == "Closed" or v._state == "Closing" then
            return true
        end
    end

    return false

end

function ObjectsManager.Backspace(self)

    for i,v in ipairs(self._panels) do v:Backspace() end

end

function ObjectsManager.Update(self,dt)

    -- Testing for the secret door
    for i,v in ipairs(self._doors) do
        if v._direction == "Secret" and v._state == "Closed" then
            if G_GameInformations:CheckSecret() then
                v._state = "Opened"
            end
        end
    end

    for i,v in ipairs(self._doors) do v:Update(dt) end
    for i,v in ipairs(self._animated) do v:Update(dt) end
end

function ObjectsManager.Action(self,gx,gy,x,y,map)

    local hasTriggeredAnAction = false

    for i,v in ipairs(self._items) do 
        if v:Action(gx,gy,x,y,map) then 
            hasTriggeredAnAction = true
        end
    end

    for i,v in ipairs(self._activators) do v:Action(gx,gy,x,y,self,map) end
    for i,v in ipairs(self._levelTransitors) do v:Action(gx,gy,x,y,map) end
    for i,v in ipairs(self._panels) do v:Action(gx,gy,x,y,map) end
    for i,v in ipairs(self._notes) do v:Action(gx,gy,x,y,map) end

    return hasTriggeredAnAction
end

function ObjectsManager.Draw(self,gridx,gridy,map)

    for i,v in ipairs(self._items) do v:Draw(gridx,gridy,map) end
    for i,v in ipairs(self._activators) do v:Draw(gridx,gridy) end
    for i,v in ipairs(self._doors) do v:Draw(gridx,gridy) end
    for i,v in ipairs(self._levelTransitors) do v:Draw(gridx,gridy) end
    for i,v in ipairs(self._animated) do v:Draw(gridx,gridy) end
    for i,v in ipairs(self._panels) do v:Draw(gridx,gridy,map) end
    for i,v in ipairs(self._notes) do v:Draw(gridx,gridy,map) end

end

function ObjectsManager.TextInput(self,t)
    for i,v in ipairs(self._panels) do v:TextInput(t) end
end

function ObjectsManager.DrawInventory(self)

    for i,v in ipairs(self._items) do
        if v._state == "Picked" then
            love.graphics.draw(v._loadedPicture
            ,G_GameInformations:TransformFromDisplay(512,(i*v._loadedPicture:getHeight())))
        end
    end

end
