--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : Panel.lua
Description : Class that represents a text note in the game
Date        : 05/12/2016

--]]

--This is the snippet for the lvl definitions


Note = Object.New()

Note_mt = {}

Note_mt.__index = Note

function Note.New(data)

    -- local newobj = {
    --     _content = "";
    -- }

    -- DS_ASSERT(nil == hdle)

    -- setmetatable(newobj,Note_mt)

    -- -- Reading properties of the item
    -- local buffer = hdle:read("*line")
    -- while buffer ~= "}" do
    --     if not Misc_IsEmpty(buffer) and not Misc_IsComment(buffer) and buffer ~= "}"then
    --         key,value = Misc_KeyValue(buffer)
    --         if key == "ID" then newobj._id = value end
    --         if key == "GRID_X" then newobj._gridX = tonumber(value) end
    --         if key == "GRID_Y" then newobj._gridY = tonumber(value) end
    --         if key == "ROOM_X" then newobj._roomX = tonumber(value) end
    --         if key == "ROOM_Y" then newobj._roomY = tonumber(value) end
    --         if key == "CONTENT" then newobj._content = love.graphics.newImage("Resources/Pics/Items/" .. value .. ".png") end
    --     end
    --     buffer = hdle:read("*line")
    -- end


    -- newobj._picture = love.graphics.newImage("Resources/Pics/Items/Note.png")
    -- newobj._pictureShown = love.graphics.newImage("Resources/Pics/Items/Note_Shown.png")
    -- newobj._shown = false
    -- newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/Paper.wav")
    -- newobj._sound:setVolume(0.6)

    local newobj = Object.New(data)

    newobj._content = love.graphics.newImage("Resources/Pics/Items/" .. data.content .. ".png")

    newobj._contentName = data.content

    newobj._picture = love.graphics.newImage("Resources/Pics/Items/Note.png")
    newobj._pictureShown = love.graphics.newImage("Resources/Pics/Items/Note_Shown.png")
    newobj._shown = false
    if love.getVersion() > 10 then 
        newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/Paper.wav", "static")
    else 
        newobj._sound = love.audio.newSource("Resources/Sounds/Sounds/Paper.wav")
    end
    newobj._sound:setVolume(0.6)


    setmetatable(newobj , Note_mt)

    return newobj

end

function Note.Dispose(self)

    self._picture = nil 
    self._pictureShown = nil 
    self._sound = nil 

end

function Note.Update(self,dt)
end

function Note.Draw(self,gridx,gridy,map)

    -- We draw the item if we are in the right grid case
    if gridx == self._gridX
    and gridy == self._gridY
    then
        if self._shown == true then
            drawx,drawy = G_GameInformations:TransformFromDisplay(128,128)
            love.graphics.draw(self._pictureShown,drawx,drawy)

            local oldr,oldg,oldb = love.graphics.getColor()
            if love.getVersion() > 10 then 
                love.graphics.setColor(0.2,0.2,0.2)
            else 
                love.graphics.setColor(20,20,20)
            end

            love.graphics.setFont(G_GameInformations._fontNotes)

            local text_to_draw = G_GameInformations:GetNote(self._contentName)

            -- love.graphics.draw(self._content,drawx,drawy)
            love.graphics.printf(text_to_draw .. "\n      Pr.Bernard" ,
                                 drawx + 60 , drawy + 20 ,140)


            love.graphics.setFont(G_GameInformations._fontText)

            love.graphics.setColor(oldr,oldg,oldb)

        else
            drawx,drawy = G_GameInformations:TransformFromDisplay(self._roomX,self._roomY)
            love.graphics.draw(self._picture,drawx,drawy)
        end
    end
end

function Note.Action(self,gx,gy,x,y,map)

    if gx ~= self._gridX and gy ~= self._gridY then return end

    -- Transforming object Coordinates
    ox,oy = G_GameInformations:TransformFromDisplay(self._roomX,self._roomY)

    if gx == self._gridX and gy == self._gridY and self._shown == true then
        self._shown = false ; self._sound:play()
        return
    end

    -- Here we action the item if we are in the good grid case and the mouse is on the object
    if x >= ox and x <= ox + self._picture:getWidth()
    and y >= oy and y <= oy + self._picture:getHeight()
    and gx == self._gridX and gy == self._gridY
    then
        if self._shown == true then
            self._shown = false
            self._sound:play()
            return true
        else
            self._shown = true
            self._sound:play()
            return true
        end
    end

    return false
end


function Note.DrawEditor(self , scrollX , scrollY)


    love.graphics.draw(self._picture 
    , (self._gridX * 256) + (self._roomX /2) + scrollX
    , (self._gridY * 256) + (self._roomY /2) + scrollY
    , 0
    , 0.5
    , 0.5 )

end

