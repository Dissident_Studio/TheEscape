


GameInformations = {}

GameInformations_mt = {}

GameInformations_mt.__index = GameInformations


function GameInformations.New()

    local hdle = love.filesystem.newFile("Game.conf")

    local json_data = G_Json.decode(tostring(hdle:read()))

    local newobj = {

        -- The version of the game
        m_gameVersion = json_data.version ;

        -- The number of rooms
        -- TODO: See if we cannot move this elsewhere
        m_numberOfRooms = json_data.number_of_rooms ;

        -- If the game is in full screen
        m_fullscreen = json_data.fullscreen ; 

        -- If we display the splash screen 
        m_splash = json_data.splash ; 

        -- Indicates the resolution of the game WITHOUT fullscreen 
        m_width = 800 ;
        m_height = 600 ;

        -- The splash picture
        m_splashPicture = love.graphics.newImage("Resources/Pics/Splash.png") ;

        -- The splash video
        -- m_splashVideo = love.graphics.newVideo(filename, true)
        -- 

        -- Informations for display
        m_display = "CENTERED" ;
        m_displayTarget = json_data.display_target ;

        -- The saved level
        m_savedLevel = nil ;

        _fontNotes = love.graphics.newFont("Resources/Fonts/Gravity-Light.otf" , 14);

        _fontText = love.graphics.newFont("Resources/Fonts/Gravity-Light.otf",24);

    }

    setmetatable(newobj , GameInformations_mt)

    if love.filesystem.exists("TheEscape.sav") then 
        local hdle = love.filesystem.newFile("TheEscape.sav" , "r")
        local json_data = G_Json.decode(tostring(hdle:read()))
        newobj.m_savedLevel = json_data.saved_level
        -- error(newobj.m_savedLevel)
        newobj.m_secrets = json_data.secrets


    else 
        local hdle , error = love.filesystem.newFile("TheEscape.sav")
        hdle:open("w")
        newobj.m_savedLevel = "Level_1"
        newobj.m_secrets = {}

        local tmp_table_for_json = {saved_level = newobj.m_savedLevel ; secrets = newobj.m_secrets}

        hdle:write(tostring(G_Json.encode(tmp_table_for_json)))
        hdle:close()
    end


    local hdle = love.filesystem.newFile("notes.json" , "r")
    local json_data = G_Json.decode(tostring(hdle:read()))

    newobj.m_notes = json_data
    
    local hdle = love.filesystem.newFile("texts.json" , "r")
    local json_data = G_Json.decode(tostring(hdle:read()))

    newobj.m_texts = json_data



    return newobj 

end

function GameInformations.ResetSave(self)

    self.m_secrets = {}
    self.m_savedLevel = "Level_1"

end

function GameInformations.GetNote(self , note)

    if G_Lang == "en" then 
        return self.m_notes[note][1].en
    else 
        return self.m_notes[note][1].fr 
    end

end

function GameInformations.GetText(self , text)

    -- for key , value in pairs(self.m_texts) do 
    --     error(key)
    -- end

    if G_Lang == "en" then 
        if self.m_texts[text] then 
            return self.m_texts[text]
        else 
            return text 
        end 
    end 

    return text 

end

function GameInformations.GetWidth(self)

    return self.m_width 

end 

function GameInformations.GetHeight(self)

    return self.m_height 

end 

function GameInformations.GetNumberOfRooms(self)

    return self.m_numberOfRooms

end

function GameInformations.GetLevelSaved(self)

    return self.m_savedLevel

end

function GameInformations.SetLevelSaved(self , level)

    self.m_savedLevel = level

end

function GameInformations.AddSecret(self , level)

    self.m_secrets[level] = true

end

function GameInformations.Save(self)

    local save_table = {
        saved_level = self.m_savedLevel ;
        secrets = self.m_secrets
    }

    local save_table_json = G_Json.encode(save_table)
    -- error(save_table.saved_level)
    local hdle = love.filesystem.newFile("TheEscape.sav" , "w")
    hdle:write(save_table_json)
    hdle:close()

end

function GameInformations.CheckSecret(self)

    local count = 0
    for level , secret in pairs(self.m_secrets) do 
        if secret == false then 
            return false 
        end 
        count = count + 1
    end 

    if count >= 21 then 
        return true 
    else 
        return false 
    end

end

function GameInformations.CountSecret(self)
            -- error(#self.m_secrets)

    local count = 0
    for level , secret in pairs(self.m_secrets) do 
        if secret == true then 
            count = count + 1 
        end 
    end 
    return count 

end

function GameInformations.TransformFromDisplay(self,x,y,s)
    
    local scale = (1 or s)
    
    if self.m_display == "CENTERED" then
        x = x + (self.m_width/2)
        y = y + (self.m_height/2)
        x = x - ((self.m_displayTarget * scale)/2)
        y = y - ((self.m_displayTarget * scale)/2)
        return x,y
    elseif self.m_display == "LEFT-CENTERED" then
        if x > (self.m_width/2) then x = x - (self.m_width/2) end
        y = y + (self.m_height/2) - (self.m_displayTarget/2)
        return x,y
    elseif self.m_display == "RIGHT-CENTERED" then
        if x < (self.m_width/2) then x = x + (self.m_width/2) end
        y = y + (self.m_height/2) - (self.m_displayTarget/2)
        return x,y
    end
end

function GameInformations.InvertTransformFromDisplay(self,x,y)
    if self.m_display == "CENTERED" then
        x = x - (self.m_width/2)
        y = y - (self.m_height/2)
        x = x + (self.m_displayTarget/2)
        y = y + (self.m_displayTarget/2)
        return x,y
    end
end