--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : Map.lua
Description : Class to manage the map
Date        : 17/10/2016

--]]

require "Classes/ObjectsManager"

Map = {}

Map_mt = {}

Map_mt.__index = Map

function Map.New()

    local newobj = {

        -- All the pictures of the rooms
        _roomsPictures = {} ;

        -- The values of the map
        _values = {} ;

        -- The values visited
        _visitedValues = {};

        -- The width and height of the map
        _width = 0 ; _height = 0 ;

        -- Coordinates in the map
        _x = 0 ; _y = 0 ;

        -- The musics of the level
        _musics = {} ;

        -- The names of the musics
        _musicsName = {} ;

        -- The directions allowed
        _allowedDirections = {} ;

        -- Indications for the transitions
        _transition = {} ;

        _red = 200;
        _ascendant = true;
        _alert = false;

        _elapsedMinutes = 14;
        _elapsedSeconds = 60;
        _elapsedMilliSeconds = 60;

        _pickingText = "";
        _pickingTime = 0;

        -- The mask that will be applied on all the sprites
        _mask = love.graphics.newImage("Resources/Pics/Shadow.png");

    }
    newobj._oldX = 0 ; newobj._oldY = 0

    newobj._transition._is = false
    newobj._transition._value = 0
    newobj._transition._direction = "None"
    if love.getVersion() > 10 then 
        newobj._transition._sound = love.audio.newSource("Resources/Sounds/Sounds/Bruit_Switch.wav" , "static")
    else 
        newobj._transition._sound = love.audio.newSource("Resources/Sounds/Sounds/Bruit_Switch.wav")
    end
    newobj._transition._sound:setVolume(0.3)
    if love.getVersion() > 10 then 
        newobj._transition._footsSound = love.audio.newSource("Resources/Sounds/Sounds/Foots.wav", "static")
    else 
        newobj._transition._footsSound = love.audio.newSource("Resources/Sounds/Sounds/Foots.wav")
    end
    newobj._transition._footsSound:setVolume(0.25)
    newobj._transition._time = 0 ; newobj._transition._state = 0

    newobj._transition._footsImageLeft = love.graphics.newImage("Resources/Pics/Foots/Foot_Left.png")
    newobj._transition._footsImageRight = love.graphics.newImage("Resources/Pics/Foots/Foot_Right.png")
    newobj._transition._footsImageDown = love.graphics.newImage("Resources/Pics/Foots/Foot_Down.png")
    newobj._transition._footsImageUp = love.graphics.newImage("Resources/Pics/Foots/Foot_Up.png")

    newobj._objectsManager = ObjectsManager.New()

    if love.getVersion() > 10 then 
        newobj._soundAlert = love.audio.newSource("Resources/Sounds/Sounds/Alert.mp3", "static")
    else 
        newobj._soundAlert = love.audio.newSource("Resources/Sounds/Sounds/Alert.mp3")
    end
    newobj._soundAlert:setVolume(0.1)

    setmetatable(newobj,Map_mt)

    newobj:LoadRoomsPictures()

    return newobj

end


function Map.Dispose(self)

    self._sound = nil 
    self._transition._sound = nil 
    self._transition._footsSound = nil 
    self._transition._footsImageDown = nil
    self._transition._footsImageUp = nil
    self._transition._footsImageLeft = nil
    self._transition._footsImageRight = nil

    self._soundAlert = nil 

    self._objectsManager:Dispose()
    
    for i , v in ipairs(self._roomsPictures) do 
        v = nil 
    end 

    self._values = nil 

end

function Map.LoadRoomsPictures(self)

    -- Loading all the rooms pictures
    for i = 0 , G_GameInformations:GetNumberOfRooms() do
        table.insert(self._roomsPictures,love.graphics.newImage("Resources/Pics/Rooms/Room_" .. i .. ".png"))
    end

    -- hdle = io.open(G_GameInformations:GetPathToGame() .. "Resources/Pics/Rooms/Rooms_List.tel")

    -- DS_ASSERT(hdle)

    -- buffer = hdle:read("*line")

    -- -- Loadin all the allowed directions
    -- while buffer ~= nil do
    --     direction = buffer:sub(1,#buffer-1)
    --     self._allowedDirections[direction] = {}
    --     while buffer ~= "}" do
    --         if buffer ~= "}" then
    --             table.insert(self._allowedDirections[direction],tonumber(buffer))
    --             buffer = hdle:read("*line")
    --         end
    --     end
    --     buffer = hdle:read("*line")
    -- end

    local hdle = love.filesystem.newFile("RoomsList.tel")

    local json_data = G_Json.decode(tostring(hdle:read()))

    self._allowedDirections["Left"] = json_data.left 
    self._allowedDirections["Right"] = json_data.right 
    self._allowedDirections["Up"] = json_data.up 
    self._allowedDirections["Down"] = json_data.down 

    hdle:close()


end

function Map.Load(self,level)


    -- OLD LOADING SYSTEM
    self._level = level

    -- print(G_GameInformations)

    -- hdle = io.open(G_GameInformations:GetPathToGame() .. "Data/" .. self._level .. "/" .. self._level .. ".tem")

    -- DS_ASSERT(hdle)

    -- buffer = hdle:read("*line")

    -- while buffer ~= nil do

    --     if not Misc_IsComment(buffer) and not Misc_IsEmpty(buffer) then

    --         if buffer == "map{" then
    --             -- Here we load the map
    --             for y = 0 , self._height-1 do
    --                 self._values[y] = {}
    --                 self._visitedValues[y] = {}
    --                 for x = 0 , self._width-1 do
    --                     self._visitedValues[y][x] = 0
    --                     self._values[y][x] = hdle:read("*number")
    --                 end
    --             end
    --         elseif buffer == "musics{" then
    --             while buffer ~= "}" do
    --                 if buffer ~= "}" and buffer ~= "musics{" then
    --                     table.insert(self._musicsName,buffer)
    --                     table.insert(self._musics,love.audio.newSource("Resources/Sounds/Musics/"..buffer..".wav"))
    --                 end
    --                 buffer = hdle:read("*line")
    --             end
    --         else
    --             if buffer ~= "}" then
    --                 key,value = Misc_KeyValue(buffer)
    --                 if key == "WIDTH" then self._width = tonumber(value) end
    --                 if key == "HEIGHT" then self._height = tonumber(value) end
    --                 if key == "ENTRY_X" then self._x = tonumber(value) end
    --                 if key == "ENTRY_Y" then self._y = tonumber(value) end
    --                 if key == "ALERT_TIME" then
    --                     self._alert =  true ; self._alertTime = tonumber(value)
    --                     self._elapsedMinutes = self._alertTime
    --                     if self._alertTime < 0 then
    --                         self._elapsedMinutes = 0
    --                         self._elapsedSeconds = self._alertTime*-100
    --                     end
    --                 end
    --             end
    --         end
    --     end
    --     buffer = hdle:read("*line")
    -- end



    -- self._visitedValues[self._y][self._x] = 1

    -- if G_GameInformations._additionalFromMap == "Alert" then
    --     if self._alert then 
    --         G_GameInformations._additionalFromMap = ""
    --         self._musics[1]:setVolume(0.5)
    --         self._musics[1]:play(1)
    --         self._musics[1]:seek(G_GameInformations._oldMusic:tell())
    --         G_GameInformations._oldMusic:stop()
    --     else
    --         G_GameInformations._oldMusic:stop()
    --     end
    -- else
    --     self._musics[1]:setVolume(0.5)
    --     self._musics[1]:play(0.7)
    -- end


    -- self._objectsManager:Load(level)

    -- 

    -- NEW LOADING SYSTEM
    local hdle = love.filesystem.newFile("Data/".. level .. "/" .. level ..".tem")

    hdle:open("r")

    local json_data = G_Json.decode(tostring(hdle:read()))

    self._width = json_data.width 
    self._height = json_data.height

    local count = 1
    for y = 0 , self._height-1 do 
        self._values[y] = {}
        self._visitedValues[y] = {}
        for x = 0 , self._width-1 do 
        self._visitedValues[y][x] = 0
        self._values[y][x] = 0
            self._values[y][x] = json_data.map[count]
            count = count + 1
        end 
    end 

    self._x = json_data.entry_x 
    self._y = json_data.entry_y


    if json_data.alert_time then 
        self._alert = true 
        self._alertTime = tonumber(json_data.alert_time)
        self._elapsedMinutes = self._alertTime
        if self._alertTime < 0 then
            self._elapsedMinutes = 0
            self._elapsedSeconds = self._alertTime*-100
        end
    end



    if love.getVersion() > 10 then 
        table.insert(self._musics , love.audio.newSource("Resources/Sounds/Musics/"..json_data.music .. ".wav", "static"))
    else 
        table.insert(self._musics , love.audio.newSource("Resources/Sounds/Musics/"..json_data.music .. ".wav"))
    end

    self._objectsManager:Load(level)

    if self._alert and G_GameInformations._additionalFromMap ~= "Alert" then
        self._pickingText = G_GameInformations:GetText("L'alerte a été déclenchée ! ")
        self._pickingTime = 2000
    end

    if G_GameInformations._additionalFromMap == "Alert" then
        if self._alert then 
            G_GameInformations._additionalFromMap = ""
            self._musics[1]:setVolume(0.5)
            self._musics[1]:play(1)
            self._musics[1]:seek(G_GameInformations._oldMusic:tell())
            G_GameInformations._oldMusic:stop()
        else
            G_GameInformations._oldMusic:stop()
        end
    else
        self._musics[1]:setVolume(0.5)
        self._musics[1]:play(0.7)
    end

end

function Map.IsDirectionAllowed(self,direction,room)

    if direction == "Left" and self._x == 0 then return false end
    if direction == "Right" and self._x == self._width then return false end
    if direction == "Up" and self._y == 0 then return false end
    if direction == "Down" and self._y == self._height then return false end

    for i,v in ipairs(self._allowedDirections[direction]) do
        -- body...
        if v == room then return true end
    end

    return false

end

function Map.Move(self,direction)
    
    local versionComplianceDivider = 1
    if love.getVersion() > 10 then 
        versionComplianceDivider = 255
    end

    if self._values[self._y][self._x] ~= -1 and self._transition._is == false and self._transition._value >= 200/versionComplianceDivider then
        if self:IsDirectionAllowed(direction,self._values[self._y][self._x]) and not self._objectsManager:IsADoorBlocking(self._x,self._y,direction) then
            self._transition._is = true
            self._oldX = self._x ; self._oldY = self._y
            self._transition._sound:play()
            self._transition._footsSound:play()
            if direction == "Left" then self._x = self._x - 1 ; self._transition._direction = "Left" end
            if direction == "Right" then self._x = self._x + 1 ; self._transition._direction = "Right" end
            if direction == "Up" then self._y = self._y - 1 ; self._transition._direction = "Up" end
            if direction == "Down" then self._y = self._y + 1 ; self._transition._direction = "Down" end
            self._visitedValues[self._y][self._x] = 1
        else
            self._pickingText = G_GameInformations:GetText("Impossible d'aller par là !")
            self._pickingTime = 750
        end
    end

end


function Map.TextInput(self,t)

    self._objectsManager:TextInput(t)

end


function Map.Debug(self)

    print("Debugging Map, level [" .. self._level .. "]")
    print("Width = " .. self._width .. " | Height = " .. self._height)
    print("Values : ")
    for y = 0 , self._height-1 do
        for x = 0 , self._width-1 do
            io.write(self._values[y][x] .. " ")
        end
        io.write("\n")
    end

end

function Map.ChangeLevel(self,level)

    for i,v in ipairs(self._musics) do
        if not self._alert then
            v:stop()
            self._soundAlert:stop()
        else
            G_GameInformations._additionalFromMap = "Alert"
            G_GameInformations._oldMusic = v
            self._soundAlert:stop()
            -- v:stop()
        end
    end

    G_GameChangingLevel._is = true
    G_GameChangingLevel._level = level

    self._transition._is = true
    self._transition._value = 0

end

function Map.Action(self,x,y)
    return self._objectsManager:Action(self._x,self._y,x,y,self)
end

function Map.Update(self,dt)

    if math.random(0, 10) == 7 then
        self._musics[1]:play(1)
    end

    if self._alert then
        self._soundAlert:play()
    end


    self._objectsManager:Update(dt)
    if self._red >= 2000 and self._ascendant == true then self._ascendant = false end
    if self._red <= 1200 and self._ascendant == false then self._ascendant = true end

    if self._ascendant == true then self._red = self._red + 40 else self._red = self._red - 40 end

    if self._elapsedMilliSeconds <= 0 then
        self._elapsedMilliSeconds = 60
        self._elapsedSeconds = self._elapsedSeconds - 1
    end
    if self._elapsedSeconds <= 0 then
        self._elapsedSeconds = 60
        self._elapsedMinutes = self._elapsedMinutes - 1
    end
    if self._elapsedMinutes <= -1 then
        --PERDU
        self._elapsedMinutes = 0
        self._elapsedSeconds = 0
        self._elapsedMilliSeconds = 1
        self._red = 0
        self._pickingTime = 2000
        self._pickingText = G_GameInformations:GetText("Temps dépassé !")
        -- error()
        self:ChangeLevel(self._level)
    end
    self._elapsedMilliSeconds = self._elapsedMilliSeconds - 1


end

function Map.Draw(self)
    
    local versionComplianceDivider = 1
    local versionComplianceValue = 10
    if love.getVersion() > 10 then 
        versionComplianceValue = 0.04
        versionComplianceDivider = 255
    end

    if not self._level then return end

    love.graphics.setColor(self._transition._value,self._transition._value,self._transition._value)
    if self._transition._is == false then
        if self._transition._value < (255 / versionComplianceDivider) then self._transition._value = self._transition._value + versionComplianceValue end
        
        if self._values[self._y][self._x] ~= -1 then
            if self._alert then
                love.graphics.setColor(self._red,0,0)
            end
            if self._level then 
                if not self._level:find("Level_Secret") then
                    local displayscale = 1.2
                    local x,y = G_GameInformations:TransformFromDisplay(0,0,displayscale,self._roomsPictures[self._values[self._y][self._x]+1])
                    love.graphics.draw(self._roomsPictures[self._values[self._y][self._x]+1]
                    ,x,y,0,displayscale,displayscale)
                end
            end
        end
        love.graphics.scale(1)
        self._objectsManager:Draw(self._x,self._y,self)
    elseif self._transition._is == true then
        if self._transition._value > 0 then self._transition._value = self._transition._value - versionComplianceValue end

        if self._transition._time > 0.06 then
            if self._transition._state+1 < 5 then
                self._transition._state = self._transition._state + 1
                self._transition._time = 0
            else
                self._transition._state = 0
                self._transition._is = false
                self._transition._time = 0
            end
        else
            self._transition._time = self._transition._time + love.timer.getDelta()
        end

        if self._values[self._oldY][self._oldX] ~= -1 then
            if self._alert then
                love.graphics.setColor(self._red,0,0)
            end
            if not self._level:find("Level_Secret") then
                local displayscale = 1.2
                local x,y = G_GameInformations:TransformFromDisplay(0,0,displayscale,self._roomsPictures[self._values[self._oldY][self._oldX]+1])
                love.graphics.draw(self._roomsPictures[self._values[self._oldY][self._oldX]+1]
                ,x , y , 0 , displayscale , displayscale)
            end
        end

        if self._transition._direction == "Left" then
            love.graphics.draw(self._transition._footsImageLeft,
            love.graphics.newQuad(self._transition._state*512,0,512,512,2560,512)
            ,G_GameInformations:TransformFromDisplay(0,0))
        elseif self._transition._direction == "Right" then
            love.graphics.draw(self._transition._footsImageRight,
            love.graphics.newQuad(self._transition._state*512,0,512,512,2560,512)
            ,G_GameInformations:TransformFromDisplay(0,0))
        elseif self._transition._direction == "Up" then
            love.graphics.draw(self._transition._footsImageUp,
            love.graphics.newQuad(self._transition._state*512,0,512,512,2560,512)
            ,G_GameInformations:TransformFromDisplay(0,0))
        elseif self._transition._direction == "Down" then
            love.graphics.draw(self._transition._footsImageDown,
            love.graphics.newQuad(self._transition._state*512,0,512,512,2560,512)
            ,G_GameInformations:TransformFromDisplay(0,0))
        end

        self._objectsManager:Draw(self._oldX,self._oldY,self)
    end

    if self._alert then
        ax,ay = G_GameInformations:TransformFromDisplay(512,512)
        love.graphics.setColor(0,255,0)
        love.graphics.print(self._elapsedMinutes .. ":" .. self._elapsedSeconds .. ":" .. self._elapsedMilliSeconds,ax,ay)
    end

    if self._pickingTime > 0 then
        tmpr,tmpg,tmpb = love.graphics.getColor()
        love.graphics.setColor(self._pickingTime,self._pickingTime,self._pickingTime)
        tx,ty = G_GameInformations:TransformFromDisplay(256,512)
        tx = tx - (#self._pickingText*10)/2
        love.graphics.print(self._pickingText,tx,ty)
        self._pickingTime = self._pickingTime - 10
        love.graphics.setColor(tmpr,tmpg,tmpb)
    end

    local shadow_x,shadow_y = G_GameInformations:TransformFromDisplay(0,0,1.2)
    love.graphics.draw(self._mask,shadow_x,shadow_y,0,1.2,1.2)

    self._objectsManager:DrawInventory()
    self:DisplayMinimap()
    
    love.graphics.scale(1)
end

function Map.DisplayMinimap(self)

    if not self._level then return end

    if self._level:find("Level_Secret") then return end

    tmpox,tmpoy = G_GameInformations:TransformFromDisplay(0,448)
    tmpox = 64

    for y=0,self._height-1 do
        for x=0,self._width-1 do
            if self._values[y][x] ~= -1 and self._visitedValues[y][x] == 1 then
                if self._alert then
                    love.graphics.setColor(self._red,0,0)
                end

                love.graphics.draw(self._roomsPictures[self._values[y][x]+1]
                ,(x*32)+tmpox,(y*32)+tmpoy,
                0,0.0625,0.0625)
                love.graphics.draw(self._mask
                ,(x*32)+tmpox,(y*32)+tmpoy,
                0,0.0625,0.0625)

                if y == self._y and x == self._x then
                    tmpr,tmpg,tmpb = love.graphics.getColor()
                    if not self._alert then love.graphics.setColor(255,0,0)
                    else love.graphics.setColor(0,255,0) end
                    love.graphics.circle("fill",(x*32)+tmpox+16,(y*32)+tmpoy+16,2)
                    love.graphics.setColor(tmpr,tmpg,tmpb)
                end
            end
        end
    end


end


function Map.Save(self)

    hdle = io.open(G_GameInformations:GetPathToGame() .. "Data/" .. self._level .. "/" .. self._level .. ".tem")

    hdle:write("WIDTH="..self._width.."\n")
    hdle:write("HEIGHT="..self._height.."\n")

    hdle:write("musics{\n")
    for i,v in ipairs(self._musicsName) do
        -- body...
        hdle:write(v .. "\n")
    end

    hdle:write("map{\n")
    for y,v in ipairs(self.height) do
        -- body...
        for x,w in ipairs(self._width) do
            -- body...
            hdle:write(self._values[y][x] .. " ")
        end
        hdle:write("\n")
    end

end
