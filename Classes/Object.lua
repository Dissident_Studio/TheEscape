



Object = {}

Object_mt = {}

Object_mt.__index = Object 


function Object.New(data)

    if not data then 
        data  = {}
    end

    local newobj = {

        -- The id of the object 
        _id = data.id or "ID";

        -- The position of the object in the grid 
        _gridX = data.grid_x or 0;
        _gridY = data.grid_y or 0;

        -- The position of the object in the room 
        _roomX = data.room_x or 0;
        _roomY = data.room_y or 0;

    }

    setmetatable(newobj , Object_mt)

    return newobj 

end