--[[
 ____  _         _   _         _      _____ _         _ _
|    \|_|___ ___|_|_| |___ ___| |_   |   __| |_ _ _ _| |_|___
|  |  | |_ -|_ -| | . | -_|   |  _|  |__   |  _| | | . | | . |
|____/|_|___|___|_|___|___|_|_|_|    |_____|_| |___|___|_|___|

Project     : Dissident Studio - The Escape
Author      : Yannis Beaux (Kranck)
File        : main.lua
Description : Main entry point for the game
Date        : 14/10/2016

    Attribution - Pas d'Utilisation Commerciale - Pas de Modification 4.0 International

--]]

G_Lang = "fr"

-- The global variables
G_GameStateManager = nil

-- Classes required
require "Classes/GameInformations"
require "Classes/Debug"
require "Classes/GameInfo"
require "Classes/File"
require "Classes/Map"
-- Classes required

-- Requiring GameStateManager
require "GameStates/GameStateManager"
require "GameStates/GameStateMainMenu"
require "GameStates/GameStateGame"
require "GameStates/GameStateEditor"
require "GameStates/GameStateEditorV2"
require "GameStates/GameStateGenerique"
-- 

-- Indicates if we start the game right in the beginning
G_START_GAME_DIRECTLY = true

-- The JSON lib
G_Json = require "Libs/Json/json"

G_Shine = require "Libs/Shine"



-- Indicates the system we are running on 
G_WINDOWS = false 
G_ANDROID = false

if love.system.getOS() == "Windows" then 
    G_WINDOWS = true 
elseif love.system.getOS() == "Android" then 
    G_ANDROID = true 
end

-- Defines
TE_EDITOR = false
BUILD = "Debug"

G_LOVE11 = true

function love.load(argtable)

    if love.getVersion() > 10 then 
        love.setDeprecationOutput(false)
    end

    if love.getVersion() > 10 then
        game_icon = love.image.newImageData("Resources/Pics/Splash.png")
    else 
        game_icon = love.graphics.newImage("Resources/Pics/Splash.png")
    end

    love.window.setTitle("The Escape - Remastered")

    if love.getVersion() > 10 then
        love.window.setIcon(game_icon)
    else 
        love.window.setIcon(game_icon:getData())
    end

    -- 
    love.filesystem.setIdentity("DissidentStudio/TheEscape" , true)

    G_GameInformations = GameInformations.New()

    -- We force the fullscreen on android so the resolution will fit the screen res
    if G_ANDROID then 
        G_GameInformations.m_fullscreen = true 
    end

    -- Setting the game cursor
    if G_WINDOWS then 
        if love.getVersion() > 10 then
            love.mouse.setCursor(love.mouse.newCursor(love.image.newImageData("Resources/Pics/Cursor.png")))
        else 
            love.mouse.setCursor(love.mouse.newCursor(love.image.newImage("Resources/Pics/Cursor.png"):getData()))
        end
    end

    -- Setting the game state manager
    G_GameStateManager = GameStateManager.New()
    G_GameStateManager:Add(GameStateSplash.New(),GameStateMainMenu.New()
    ,GameStateGame.New(),GameStateEditor.New(),GameStateEditorV2.New(),GameStateGenerique.New())

    -- Setting the window according to the GameInfo

    if TE_EDITOR then
        G_GameStateManager:Push("GameStateEditorV2")
    else
        if not G_START_GAME_DIRECTLY then 
            G_GameStateManager:Push("GameStateSplash")
        else 
            love.window.setMode(800,600,{fullscreen = true , fullscreentype = "desktop"})

            G_Scanlines = G_Shine.scanlines()
            G_Chroma = G_Shine.separate_chroma()
            G_Chroma.angle = 0
            G_Chroma.radius = 0

            G_PostEffect = G_Scanlines:chain(G_Chroma)


            G_GameInformations.m_width = love.graphics.getWidth()
            G_GameInformations.m_height = love.graphics.getHeight()
            G_GameStateManager:Push("GameStateMainMenu")
        end
    end

end

function love.update(dt)
    G_GameStateManager:Update(dt)
end

function love.draw()
    if G_PostEffect then 
        G_PostEffect:draw(function()
            G_GameStateManager:Draw()
        end)
    else 
        G_GameStateManager:Draw()
    end
end

function love.keypressed(key,scancode,isrepeat)
    G_GameStateManager:KeyPressed(key,scancode,isrepeat)
end

function love.mousepressed(x,y,button,istouch)
    G_GameStateManager:MousePressed(x,y,button,istouch)
end

function love.wheelmoved(x,y)
    G_GameStateManager:WheelMoved(x,y)
end

function love.filedropped(file)
    G_GameStateManager:FileDropped(file)
end

function love.resize(w,h)
    G_GameStateManager:Resize(w,h)
end

function love.textinput(t)
    -- error(t)
    G_GameStateManager:TextInput(t)
end
